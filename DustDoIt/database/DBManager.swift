//
//  DBManager.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 21..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import RealmSwift

/*
 미세먼지
 */
class DBManager {
    private var db: Realm
    static let shared = DBManager()
    
    private init() {
//        db = try! Realm()
        db = try! Realm(fileURL: FileManager
            .default
            .containerURL(forSecurityApplicationGroupIdentifier: "group.dustDoItTodayExtension")!
            .appendingPathComponent("db.realm"))
    }
    
    func getStationsWithName(name: String) -> Results<Station>? {
        return db.objects(Station.self)
            .filter("stationName CONTAINS[c] %@ OR addr CONTAINS[c] %@", name, name)
            .sorted(byKeyPath: "stationName", ascending: true)
    }
    
    func getAllStations() -> Results<Station>? {
        return db.objects(Station.self)
    }
    
    func insertData(stations: [Station])   {
        try! db.write {
            for station in stations {
                station.id = station.incrementID()
                db.add(station, update: true)
            }
            logger("insert stations done!!")
        }
    }
    
    func addData(stations: [Station])   {
        try! db.write {
            for station in stations {
                station.id = station.incrementID()
                db.add(station, update: false)
            }
        }
    }
    
    func deleteAllStations()  {
        try! db.write {
            if let stations = getAllStations() {
                db.delete(stations)
            }
            logger("delete all stations done!!")
        }
    }
    
    func deleteAllDb()  {
        try! db.write {
            db.deleteAll()
        }
    }

    func deleteFromDb(station: Station)   {
        try! db.write {
            db.delete(station)
        }
    }
}

/*
 주소 저장
 */

extension DBManager {
    
    func insertData(districts: [AdminDistrict])   {
        logger("districts.count = \(districts.count)")
        try! db.write {
            for district in districts {
                db.add(district, update: false)
                logger("district = \(district)")
            }
            logger("insert districts done!!")
        }
    }
    
    func getDistrictsWith(si: String, gu: String) -> Results<AdminDistrict>? {
        return db.objects(AdminDistrict.self)
            .filter("level1 CONTAINS[c] %@ AND level2 CONTAINS[c] %@", si, gu)
            .sorted(byKeyPath: "level1", ascending: true)
    }
    
    func getAllDistricts() -> Results<AdminDistrict>? {
        return db.objects(AdminDistrict.self)
    }
    
    func deleteAllDistricts()  {
        try! db.write {
            if let district = getAllDistricts() {
                db.delete(district)
            }
            logger("delete all district done!!")
        }
    }
}


/*
 날씨 예보
 */
//extension DBManager {
//    
//    // forecast
//    func insertData(forecasts: [WeatherForeCast])   {
//        try! db.write {
//            for forecast in forecasts {
//                db.add(forecast, update: true)
//            }
//            logger("insert forecasts done!!")
//        }
//    }
//    
//    func getAllForecasts() -> Results<WeatherForeCast>? {
//        return db.objects(WeatherForeCast.self)
//    }
//    
//    func deleteAllForecasts()  {
//        try! db.write {
//            if let forecasts = getAllForecasts() {
//                db.delete(forecasts)
//            }
//            logger("delete all forecast done!!")
//        }
//    }
//    
//    // city
//    func insertData(city: City)   {
//        try! db.write {
//            db.add(city, update: true)
//            logger("insert city done!!")
//        }
//    }
//    
//    func getAllCity() -> Results<City>? {
//        return db.objects(City.self)
//    }
//
//    func deleteAllCity()  {
//        try! db.write {
//            if let city = getAllCity() {
//                db.delete(city)
//            }
//            logger("delete all city done!!")
//        }
//    }
//}







