//
//  PreventFrequentlyCall.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 11..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation

class PreventFrequentlyCall {
    static let shared = PreventFrequentlyCall()
    
    private init() {}
    let key_getCityCallTime: String = "getCityCallTime"
    
    func canCallApi() -> Bool {
        let prevCallTime = UserDefaults.standard.object(forKey: key_getCityCallTime) as? Date
        
        if let elapsedTime = prevCallTime?.timeIntervalSinceNow {
            if abs(Int(elapsedTime)) > 60 { // 이전 호출후 1분 지났을때
                setCalltime()
                return true
            } else {
                logger("elapsedTime = \(abs(Int(elapsedTime)))")
                return false
            }
        } else { // 호출이 처음일때
            setCalltime()
            return true
        }
    }
    
    private func setCalltime() {
        UserDefaults.standard.set(Date(), forKey: key_getCityCallTime)
    }
}
