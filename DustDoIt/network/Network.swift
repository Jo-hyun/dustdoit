//
//  Network.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 25..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
import AlamofireObjectMapper

public enum NetCallBack {
    case reqExceeded, noInternet, networkFailure
    case stationCount, station, measureInfos, nearStation, pm10Forecasts, pm25Forecasts
    case address
    case shortTermForecast, spaceDataForecast
}

protocol NetworkCallback {
    func getResult(name: NetCallBack, result: Bool)
}

fileprivate protocol Network {
    associatedtype TYPE
    
    var data: TYPE? {get}
    func getMiddelUrl() -> String
    func getParmaString() -> String
    func getBaseURL() -> String
    func returnData() -> TYPE?
}

fileprivate class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class GetData<T>: Network where T: Mappable {
    
    typealias TYPE = T
    
    var data: T?
    var networkCallback: NetworkCallback?
    var callbackName: NetCallBack = .noInternet
    var isRequesting: Bool = false
    
    func setNetworkCallback(callback: NetworkCallback, callbackName: NetCallBack) {
        self.networkCallback = callback
        self.callbackName = callbackName
    }
    
    func getBaseURL() -> String {
        return ""
    }
    
    func getMiddelUrl() -> String {
        return ""
    }
    
    func getParmaString() -> String {
        return ""
    }
    
    func returnData() -> T? {
        return self.data
    }
    
    // api 요청횟수 초과됬을때 처리방법??
    func startRequest() {
        
        guard let networkCallback = self.networkCallback else {
            logger("you should set networkCallback before call startRequest!!")
            return
        }
        
        if !Connectivity.isConnectedToInternet() {
            networkCallback.getResult(name: .noInternet, result: false)
            return
        }
        
        if isRequesting {
            return
        }
        self.isRequesting = true
        
        logger(self.getBaseURL() + self.getMiddelUrl() + self.getParmaString())

        let sessionManager = Alamofire.SessionManager.default
        sessionManager.request(self.getBaseURL() + self.getMiddelUrl() + self.getParmaString())
            .responseObject { (response: DataResponse<TYPE>) in
                
                switch (response.result) {
                case .success:
                    if let data = response.result.value {
                        self.data = data
                        logger("sessionManager success \(self.callbackName)")
//                        logger("response.result.value! ========= \(String(describing: self.data))")
                        networkCallback.getResult(name: self.callbackName, result: true)
                    } else {
                        self.data = nil
                        logger("sessionManager success networkFailure")
//                        logger("response.result.value! ========= \(String(describing: self.data))")
                        networkCallback.getResult(name: .networkFailure, result: false)
                    }
                case .failure:
                    logger("sessionManager failure")
                    if let error = response.error as? AFError {
                        switch (error) {
                        case .invalidURL(let url):
                            logger("Invalid URL: \(url) - \(error.localizedDescription)")
                        case .parameterEncodingFailed(let reason):
                            logger("Parameter encoding failed: \(error.localizedDescription)")
                            logger("Failure Reason: \(reason)")
                        case .multipartEncodingFailed(let reason):
                            logger("Multipart encoding failed: \(error.localizedDescription)")
                            logger("Failure Reason: \(reason)")
                        case .responseValidationFailed(let reason):
                            logger("Response validation failed: \(error.localizedDescription)")
                            logger("Failure Reason: \(reason)")
                            
                            switch reason {
                            case .dataFileNil, .dataFileReadFailed:
                                logger("Downloaded file could not be read")
                            case .missingContentType(let acceptableContentTypes):
                                logger("Content Type Missing: \(acceptableContentTypes)")
                            case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                logger("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                            case .unacceptableStatusCode(let code):
                                logger("Response status code was unacceptable: \(code)")
                            }
                        case .responseSerializationFailed(let reason):
                            logger("Response serialization failed: \(error.localizedDescription)")
                            logger("Failure Reason: \(reason)")
                        }
                    } else if let error = response.error as? URLError {
                        logger("URLError occurred: \(error)")
                    } else {
                        logger("Unknown error: \(String(describing: response.error))")
                    }
                    networkCallback.getResult(name: .networkFailure, result: false)
                }
                self.isRequesting = false
        }
    }
}


















