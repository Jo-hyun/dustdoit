//
//  Apis.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 9..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation

/*
 미세먼지
 */
class GetNearStation: GetData<NearStations> {
    private var tmX: String!
    private var tmY: String!
    
    init(tmX: String, tmY: String) {
        self.tmX = tmX
        self.tmY = tmY
    }
    
    override func getBaseURL() -> String {
        return DustNetConstant.AIR_KOREA_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += DustNetConstant.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += DustNetConstant.PARAM_RETURNTYPE + "=" + DustNetConstant.VALUE_JSON + "&"
        str += DustNetConstant.PARAM_NUMOFROWS + "=" + "1" + "&"
        str += DustNetConstant.PARAM_TMX + "=" + self.tmX + "&"
        str += DustNetConstant.PARAM_TMY + "=" + self.tmY
        return str
    }
    
    override func getMiddelUrl() -> String {
        return DustNetConstant.GET_NEAR_STATIONS
    }
}

class GetStationCount: GetData<StationList> {
    
    override func getBaseURL() -> String {
        return DustNetConstant.AIR_KOREA_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += DustNetConstant.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += DustNetConstant.PARAM_RETURNTYPE + "=" + DustNetConstant.VALUE_JSON + "&"
        str += DustNetConstant.PARAM_NUMOFROWS + "=" + "1"
        return str
    }
    
    override func getMiddelUrl() -> String {
        return DustNetConstant.GET_STATION_LIST_BY_NAME
    }
}

class GetStations: GetData<StationList> {
    private var count: Int?
    
    init(count: Int) {
        self.count = count
    }
    
    override func getBaseURL() -> String {
        return DustNetConstant.AIR_KOREA_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += DustNetConstant.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += DustNetConstant.PARAM_RETURNTYPE + "=" + DustNetConstant.VALUE_JSON + "&"
        str += DustNetConstant.PARAM_NUMOFROWS + "=" + String(count!)
        return str
    }
    
    override func getMiddelUrl() -> String {
        return DustNetConstant.GET_STATION_LIST_BY_NAME
    }
}

class GetMeasureInfos: GetData<MeasureInfos> {
    private var stationName: String?
    
    init(stationName: String) {
        self.stationName = stationName
    }
    
    override func getBaseURL() -> String {
        return DustNetConstant.AIR_KOREA_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += DustNetConstant.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += DustNetConstant.PARAM_DATATERM + "=" + DustNetConstant.VALUE_DATATERM + "&"
        str += DustNetConstant.PARAM_RETURNTYPE + "=" + DustNetConstant.VALUE_JSON + "&"
        str += DustNetConstant.PARAM_STATIONNAME + "=" + stationName!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! + "&"
        str += DustNetConstant.PARAM_VER + "=" + DustNetConstant.VALUE_VER + "&"
        str += DustNetConstant.PARAM_NUMOFROWS + "=" + "1"
        return str
    }
    
    override func getMiddelUrl() -> String {
        return DustNetConstant.GET_STATION_MEASURE_INFO
    }
}

class GetForecastInfo: GetData<DustForeCasts> {
    private var type: String = "PM10"
    private var today: Bool = true
    private let format = "yyyy-MM-dd"
    
    init(type: String = "PM10", today: Bool) {
        self.type = type
        self.today = today
    }
    
    override func getBaseURL() -> String {
        return DustNetConstant.AIR_KOREA_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += DustNetConstant.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += DustNetConstant.PARAM_SEARCHDATE + "=" + (today ? Date.todayDateWith(format: format) : Date.yesterdayDateWith(format: format)) + "&"
        str += DustNetConstant.PARAM_INFORMCODE + "=" + type + "&"
        str += DustNetConstant.PARAM_RETURNTYPE + "=" + DustNetConstant.VALUE_JSON + "&"
        str += DustNetConstant.PARAM_NUMOFROWS + "=" + "1"
        return str
    }
    
    override func getMiddelUrl() -> String {
        return DustNetConstant.GET_FORECAST_INFO
    }
}

/*
 vworld - 현재 좌표로 주소 불러오기
 */
class GetAddress: GetData<Address> {
    private var x: Double = 0
    private var y: Double = 0
    
    init(x: Double, y: Double ) {
        self.x = x
        self.y = y
    }
    
    override func getBaseURL() -> String {
        return VWorldNetConstant.VWORLD_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += VWorldNetConstant.PARAM_SERVICE + "=" + VWorldNetConstant.VALUE_SERVICE + "&"
        str += VWorldNetConstant.PARAM_API_KEY + "=" + V_WORLD_API_KEY + "&"
        str += VWorldNetConstant.PARAM_REQUEST + "=" + VWorldNetConstant.VALUE_REQUEST + "&"
        str += VWorldNetConstant.PARAM_FORMAT + "=" + VWorldNetConstant.VALUE_FORMAT + "&"
        str += VWorldNetConstant.PARAM_POINT + "=" + "\(self.x),\(self.y)" + "&"
        str += VWorldNetConstant.PARAM_CRS + "=" + VWorldNetConstant.VALUE_CRS + "&"
        str += VWorldNetConstant.PARAM_TYPE + "=" + VWorldNetConstant.VALUE_TYPE + "&"
        str += VWorldNetConstant.PARAM_ZIPCODE + "=" + VWorldNetConstant.VALUE_ZIPCODE + "&"
        str += VWorldNetConstant.PARAM_SIMPLE + "=" + VWorldNetConstant.VALUE_SIMPLE
        return str
    }
    
    override func getMiddelUrl() -> String {
        return VWorldNetConstant.ADRESS
    }
}

/*
 동네예보
 */
class GetSpaceDataForecast: GetData<SpaceDataForecasts> {
    private var baseDate: String = ""
    private var baseTime: String = ""
    private var x: String = "54"
    private var y: String = "124"
    
    init(baseDate: String, baseTime: String, x: String, y: String) {
        self.baseDate = baseDate
        self.baseTime = baseTime
        self.x = x
        self.y = y
    }
    
    override func getBaseURL() -> String {
        return WeatherNetConstants.WEATHER_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += WeatherNetConstants.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += WeatherNetConstants.PARAM_BASE_DATE + "=" + self.baseDate + "&"
        str += WeatherNetConstants.PARAM_BASE_TIME + "=" + self.baseTime + "&"
//        str += WeatherNetConstants.PARAM_BASE_TIME + "=" + "0200" + "&"
        str += WeatherNetConstants.PARAM_NX + "=" + self.x + "&"
        str += WeatherNetConstants.PARAM_NY + "=" + self.y + "&"
        str += WeatherNetConstants.PARAM_TYPE + "=" + WeatherNetConstants.VALUE_TYPE + "&"
        str += WeatherNetConstants.PARAM_NUMOFROWS + "=" + WeatherNetConstants.VALUE_NUMOFROWS
        return str
    }
    
    override func getMiddelUrl() -> String {
        return WeatherNetConstants.FORECAST_SPACEDATA
    }
}

class GetShortTermForecast: GetData<ShortTermForecast> {
    private var baseDate: String = ""
    private var baseTime: String = ""
    private var x: String = "54"
    private var y: String = "124"
    
    init(baseDate: String, baseTime: String, x: String, y: String) {
        self.baseDate = baseDate
        self.baseTime = baseTime
        self.x = x
        self.y = y
    }
    
    override func getBaseURL() -> String {
        return WeatherNetConstants.WEATHER_BASE_URL
    }
    
    override func getParmaString() -> String {
        var str = "?"
        str += WeatherNetConstants.PARAM_SERVICE_KEY + "=" + SERVICE_KEY + "&"
        str += WeatherNetConstants.PARAM_BASE_DATE + "=" + self.baseDate + "&"
        str += WeatherNetConstants.PARAM_BASE_TIME + "=" + self.baseTime + "&"
        str += WeatherNetConstants.PARAM_NX + "=" + self.x + "&"
        str += WeatherNetConstants.PARAM_NY + "=" + self.y + "&"
        str += WeatherNetConstants.PARAM_TYPE + "=" + WeatherNetConstants.VALUE_TYPE
        return str
    }
    
    override func getMiddelUrl() -> String {
        return WeatherNetConstants.FORECAST_GRIB
    }
}














