//
//  IntroViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 20..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit
import RealmSwift

class IntroViewController: UIViewController, NetworkCallback {
    
    private lazy var introLabel: UILabel = {
        let label = UILabel()
        label.text = "DUST DO IT"
        label.textAlignment = .center
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 50, weight: .ultraLight)
        return label
    }()
    
    private var stationCount: GetStationCount!
    private var stations: GetStations!
    private var count: Int = 0
    private var networkFailCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger("viewDidLoad")
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = .white
        
        self.view.addSubview(introLabel)
        introLabel.fillSuperViewSafeArea()
        getStationCount()
    }
    
    func getResult(name: NetCallBack, result: Bool) {
        switch name {
        case .stationCount:
            guard let data = self.stationCount.returnData() else {return}
            guard let count = data.totalCount else {return}
            self.count = count
            
            // check station count in db
            let stations = DBManager.shared.getAllStations()
            if let stations = stations {
                logger("db stations.count ======== \(stations.count), network stations.count ========= \(count)")
                if stations.count != count {
                    self.getStations(count: count)
                } else {
                    createAddressDB()
                }
            } else {
                self.getStations(count: count)
            }
        case .station:
            guard let data = self.stations.returnData() else {return}
            guard let stations = data.list else {
                return
            }
            
            DBManager.shared.deleteAllStations()
            DBManager.shared.insertData(stations: stations)
            
            createAddressDB()
            
        case .noInternet:
            let alert = UIAlertController(title: "네트워크", message: "인터넷 연결을 확인해 주세요", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.getStationCount()
            }))
            self.present(alert, animated: true, completion: nil)
//            LoadingIndicator.hide()
        case .networkFailure:
            var message: String = "서버와 통신을 완료하지 못했습니다."
            if (networkFailCount != 0 && networkFailCount % 5 == 0) {
                message = "서버가 응답이 없습니다.\n잠시후 다시 시도해 주세요."
            }
            
            let alert = UIAlertController(title: "통신장애", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.networkFailCount += 1
                self.getStationCount()
            }))
            self.present(alert, animated: true, completion: nil)
//            LoadingIndicator.hide()
        default: break
        }
    }
    
    private func getStationCount() {
//        LoadingIndicator.show()
        
        stationCount = GetStationCount()
        stationCount.setNetworkCallback(callback: self, callbackName: .stationCount)
        stationCount.startRequest()
    }
    
    private func getStations(count: Int) {
        stations = GetStations(count: count)
        stations.setNetworkCallback(callback: self, callbackName: .station)
        stations.startRequest()
    }
    
    private func showErrorAlert(title: String, msg: String) {
//        LoadingIndicator.hide()
        logger("\(title) - \(msg)")
    }
    
    private func createAddressDB() {
//        LoadingIndicator.show("주소데이터 생성중...")
        if let filePath = Bundle.main.path(forResource: "code", ofType: "csv") {
            ReadCSV.shared.read(filePath: filePath) {
//                LoadingIndicator.hide()
                DispatchQueue.main.async {
                    logger("IntroViewController go MainViewController")
                    self.navigationController?.pushViewController(MainViewController(), animated: false)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
