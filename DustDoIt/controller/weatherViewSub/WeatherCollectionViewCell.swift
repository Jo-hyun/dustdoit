//
//  WeatherCollectionViewCell.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 20..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    
    var data: [String: [SpaceDataItem]]?
    
    var v00: [SpaceDataItem]?
    var v03: [SpaceDataItem]?
    var v06: [SpaceDataItem]?
    var v09: [SpaceDataItem]?
    var v12: [SpaceDataItem]?
    var v15: [SpaceDataItem]?
    var v18: [SpaceDataItem]?
    var v21: [SpaceDataItem]?
    
    let timeText = ["0시", "3시", "6시", "9시", "12시", "15시", "18시", "21시"]
    var parentStacks: [UIStackView] = []
    
    func setUp() {
        setFrame()
        parseData()
        initLayout()
    }
    
    private func setFrame() {
        self.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.borderWidth = 0.5
        
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        self.backgroundColor = .white
    }
    
    private func parseData() {
        if let data = self.data {
//            logger("data = \(data)")
            self.v00 = data["0000"]
            self.v03 = data["0300"]
            self.v06 = data["0600"]
            self.v09 = data["0900"]
            self.v12 = data["1200"]
            self.v15 = data["1500"]
            self.v18 = data["1800"]
            self.v21 = data["2100"]
        }
    }
    
    var dateLabel: UILabel!
    private func initLayout() {
        dateLabel = getContentLabel(text: "date", small: false)
        dateLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        dateLabel.textAlignment = .left
        self.addSubview(dateLabel)
        dateLabel.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 5), size: CGSize(width: 0, height: 24))
        
        let timeStack = getEmptyStack()
        self.addSubview(timeStack)
        timeStack.anchor(top: dateLabel.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 20))
        
        let skyImageStack = getEmptyStack()
        self.addSubview(skyImageStack)
        skyImageStack.anchor(top: timeStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 25, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 30))
        
        let popStack = getEmptyStack()
        self.addSubview(popStack)
        popStack.anchor(top: skyImageStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 15))
        
        let temperatureStack = getEmptyStack()
        self.addSubview(temperatureStack)
        temperatureStack.anchor(top: popStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 25, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 15))
        
        let windStack = getEmptyStack()
        self.addSubview(windStack)
        windStack.anchor(top: temperatureStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 25, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 15))
        
        let humidityImageStack = getEmptyStack()
        self.addSubview(humidityImageStack)
        humidityImageStack.anchor(top: windStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 25, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 30))
        
        let humidityStack = getEmptyStack()
        self.addSubview(humidityStack)
        humidityStack.anchor(top: humidityImageStack.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5), size: CGSize(width: 0, height: 15))
        
        parentStacks = [skyImageStack, popStack, temperatureStack, windStack, humidityImageStack, humidityStack]
        let timelyData = [v00, v03, v06, v09, v12, v15, v18, v21]
        for i in 0..<8 {
            timeStack.addArrangedSubview(getContentLabel(text: timeText[i], small: false))
            parseTimely(itemArr: timelyData[i])
        }
    }
    
    private func getEmptyStack() -> UIStackView {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        
        return stack
    }
    
    private func getImageView(img: UIImage) -> UIImageView {
        let view = UIImageView(image: img)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
    
    private func getContentLabel(text: String, small: Bool) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.textAlignment = .center
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: small ? 10 : 12, weight: .ultraLight)
        
        return label
    }
    
    private func parseTimely(itemArr: [SpaceDataItem]?) {
        
//        logger("itemArr = \(itemArr)")
        if let spaceItmes = itemArr {
            var dateStr: String = ""
            
            var temperatureText: String = ""
            var popText: String = ""
            var windText: String = ""
            var skyValue: Int = 0
            var ptyValue: Int = 0
            var humidity: Int = 0
            
            for item in spaceItmes {
                if let category = item.category {
                    if category == "T3H" { // 3시간 기온
                        if let t = item.fcstValue {
                            temperatureText = "\(Int(t))˚"
                        }
                        if let d = item.fcstDate {
                            var todayStr = ""
                            let dataDay = Int(String(d).suffix(2))
                            if let unwrappedDataDay = dataDay {
                                if unwrappedDataDay == Date.getTodayDay() {
                                    todayStr = "(오늘)"
                                } else if unwrappedDataDay == Date.getTomorrowDay() {
                                    todayStr = "(내일)"
                                } else if unwrappedDataDay == Date.getTomorrowAfterOneDay() {
                                    todayStr = "(모레)"
                                }
                            }
                            
                            let dateformatter = DateFormatter()
                            dateformatter.dateFormat = "yyyyMMdd"
                            let date = dateformatter.date(from: String(d))
                            dateformatter.dateFormat = "yyyy-MM-dd"
                            guard let showDate = date else {return}
                            
                            dateStr = dateformatter.string(from: showDate) + " \(todayStr)"
                        }
                    } else if category == "POP" { // 강수 확률
                        if let p = item.fcstValue {
                            popText = "\(p)%"
                        }
                    } else if category == "PTY" { // 강수 형태
                        if let p = item.fcstValue {
                            ptyValue = p
                        }
                    } else if category == "SKY" { // 하늘상태
                        if let s = item.fcstValue {
                            skyValue = s
                        }
                    } else if category == "WSD" { // 풍속
                        if let w = item.fcstValue {
                            windText = "\(Int(w))m/s"
                        }
                    } else if category == "REH" { // 습도
                        if let h = item.fcstValue {
                            humidity = h
                        }
                    }
                }
            }
            
            // parentStacks = [skyImageStack, popStack, temperatureStack, windStack, humidityImageStack, humidityStack]
            dateLabel.text = dateStr
            parentStacks[0].addArrangedSubview(ptyValue != 0 ? getRainOrSnowImageView(val: ptyValue) : getSkyImgaeView(val: skyValue))
            parentStacks[1].addArrangedSubview(getContentLabel(text: popText, small: true))
            parentStacks[2].addArrangedSubview(getContentLabel(text: temperatureText, small: false))
            parentStacks[3].addArrangedSubview(getContentLabel(text: windText, small: true))
            parentStacks[4].addArrangedSubview(getHumidityImageView(val: humidity))
            parentStacks[5].addArrangedSubview(getContentLabel(text: "\(humidity)%", small: true))
        } else {
            parentStacks[0].addArrangedSubview(UIImageView())
            parentStacks[1].addArrangedSubview(getContentLabel(text: "", small: false))
            parentStacks[2].addArrangedSubview(getContentLabel(text: "", small: false))
            parentStacks[3].addArrangedSubview(getContentLabel(text: "", small: false))
            parentStacks[4].addArrangedSubview(UIImageView())
            parentStacks[5].addArrangedSubview(getContentLabel(text: "", small: false))
        }
    }
    
    private func getSkyImgaeView(val: Int) -> UIImageView {
        var img: UIImage = #imageLiteral(resourceName: "weather_sun")
        
        switch val {
        case 1:
            img = #imageLiteral(resourceName: "weather_sun")
        case 2:
            img = #imageLiteral(resourceName: "weather_cloud1")
        case 3:
            img = #imageLiteral(resourceName: "weather_cloud2")
        case 4:
            img = #imageLiteral(resourceName: "weather_cloud3")
        default:
            img = #imageLiteral(resourceName: "weather_sun")
        }
        return self.getImageView(img: img)
    }
    
    private func getRainOrSnowImageView(val: Int) -> UIImageView {
        var img: UIImage = #imageLiteral(resourceName: "weather_rain")
        
        switch val {
        case 1:
            img = #imageLiteral(resourceName: "weather_rain")
        case 2:
            img = #imageLiteral(resourceName: "weather_rainSnow")
        case 3:
            img = #imageLiteral(resourceName: "weather_snow")
        default:
            img = #imageLiteral(resourceName: "weather_rain")
        }
        return self.getImageView(img: img)
    }
    
    private func getHumidityImageView(val: Int) -> UIImageView {
        var img: UIImage = #imageLiteral(resourceName: "humidity_0")
        
        switch round(Double(val / 10)) {
        case 1:
            img = #imageLiteral(resourceName: "humidity_10")
        case 2:
            img = #imageLiteral(resourceName: "humidity_20")
        case 3:
            img = #imageLiteral(resourceName: "humidity_30")
        case 4:
            img = #imageLiteral(resourceName: "humidity_40")
        case 5:
            img = #imageLiteral(resourceName: "humidity_50")
        case 6:
            img = #imageLiteral(resourceName: "humidity_60")
        case 7:
            img = #imageLiteral(resourceName: "humidity_70")
        case 8:
            img = #imageLiteral(resourceName: "humidity_80")
        case 9:
            img = #imageLiteral(resourceName: "humidity_90")
        case 10:
            img = #imageLiteral(resourceName: "humidity_100")
        default:
            img = #imageLiteral(resourceName: "humidity_50")
        }
        return self.getImageView(img: img)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
