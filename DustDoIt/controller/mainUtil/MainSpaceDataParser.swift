//
//  MainSpaceDataParser.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 19..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation

class MainSpaceDataParser {
    private var spaceDataItems: [SpaceDataItem]!
    private var processedSpaceData: ProcessedSpaceData!
    private var isParsingDone: Bool = false
    
    init(spaceDataItems: [SpaceDataItem]) {
        self.spaceDataItems = spaceDataItems
        self.processedSpaceData = ProcessedSpaceData()
        startParsing()
    }
    
    private func startParsing() {
        var tempTodayArr: [SpaceDataItem] = []
        var tempAfter1Arr: [SpaceDataItem] = []
        var tempAfter2Arr: [SpaceDataItem] = []
        
        var tempTodayDic: [String: [SpaceDataItem]] = [:]
        var tempAfter1Dic: [String: [SpaceDataItem]] = [:]
        var tempAfter2Dic: [String: [SpaceDataItem]] = [:]
        
        for data in spaceDataItems {
//            guard let baseDate = data.baseDate else {return}
            guard let fcstDate = data.fcstDate else {return}
            let monthAndDate = String(fcstDate).suffix(4)
            
//            guard let month = Int(monthAndDate.prefix(2)) else {return}
            guard let day = Int(monthAndDate.suffix(2)) else {return}
            
            let today = Date.getTodayDay()
            let tomorrow = Date.getTomorrowDay()
            let tomorrowAfterOne = Date.getTomorrowAfterOneDay()
            var key: String = ""
            
            // time comes int or string
            if let kkey = data.fcstTime as? Int {
                key = String(kkey)
            } else if let kkey = data.fcstTime as? String {
                key = kkey
            }
            
            if day == today { // today
                tempTodayArr.append(data)
                tempTodayDic.updateValue(tempTodayArr, forKey: key)
            }
            
            if day == tomorrow { // after1
                tempAfter1Arr.append(data)
                tempAfter1Dic.updateValue(tempAfter1Arr, forKey: key)
            }
            
            if day == tomorrowAfterOne { // after2
                tempAfter2Arr.append(data)
                tempAfter2Dic.updateValue(tempAfter2Arr, forKey: key)
            }
        }
        
        processedSpaceData.setToday(spaceDataItems: tempTodayDic)
        processedSpaceData.setAfter1(spaceDataItems: tempAfter1Dic)
        processedSpaceData.setAfter2(spaceDataItems: tempAfter2Dic)
        
        isParsingDone = true
    }
    
    func getProcessedData() -> ProcessedSpaceData {
        return isParsingDone ? self.processedSpaceData : ProcessedSpaceData()
    }
}

class ProcessedSpaceData {
    private var today: [String: [SpaceDataItem]] = [:]
    private var after1: [String: [SpaceDataItem]] = [:]
    private var after2: [String: [SpaceDataItem]] = [:]
    
    func getToday() -> [String: [SpaceDataItem]] {
        return today
    }
    
    func getAfter1() -> [String: [SpaceDataItem]] {
        return after1
    }
    
    func getAfter2() -> [String: [SpaceDataItem]] {
        return after2
    }
    
    func setToday(spaceDataItems: [String: [SpaceDataItem]]) {
        today = spaceDataItems
    }
    
    func setAfter1(spaceDataItems: [String: [SpaceDataItem]]) {
        after1 = spaceDataItems
    }
    
    func setAfter2(spaceDataItems: [String: [SpaceDataItem]]) {
        after2 = spaceDataItems
    }
}
