//
//  WeatherViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 10..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit
import DeckTransition
import UPCarouselFlowLayout

class WeatherViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    private var dismissArrow: UIImageView = {
        let view = UIImageView(image: #imageLiteral(resourceName: "arrow_down"))
        view.isUserInteractionEnabled = true
        view.alpha = 0.2
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var currentWeatherContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private var collectionViewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var districtName: [String: String] = [:]
    var shortTermItemArr: [ShortTermItem]?
    var processedSpaceData: ProcessedSpaceData?
    
    var collectionData: [[String: [SpaceDataItem]]] = []
    
    private let reuseIdentifier = "weatherCell"
    private var currentWeatherView: CurrentWeatherView!
    private var weatherCollectionView: UICollectionView!
    
    var itemHeight = CGFloat(0)
    var itemWidth = CGFloat(0)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.barStyle = .black
        self.modalPresentationCapturesStatusBarAppearance = true
        
        setLayout()
    }
    
    private func getTitleLabel(text: String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .left
        
        return label
    }
    
    @objc func tapDownArrow() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setLayout() {
        
        self.view.addSubview(dismissArrow)
        
        dismissArrow.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10).isActive = true
        dismissArrow.heightAnchor.constraint(equalToConstant: 18).isActive = true
        dismissArrow.widthAnchor.constraint(equalTo: dismissArrow.heightAnchor, multiplier: 2).isActive = true
        dismissArrow.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapDownArrow))
        dismissArrow.addGestureRecognizer(tap)
        
        let currentWeatherTitle = getTitleLabel(text: "현재 날씨")
        self.view.addSubview(currentWeatherTitle)
        currentWeatherTitle.anchor(top: dismissArrow.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 0), size: CGSize(width: 0, height: 40))
        
        currentWeatherView = CurrentWeatherView()
        self.view.addSubview(currentWeatherContainer)
        currentWeatherContainer.anchor(top: currentWeatherTitle.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor,padding: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0), size: CGSize(width: self.view.frame.width, height: 70))
        
        currentWeatherContainer.addSubview(currentWeatherView)
        currentWeatherView.fillSuperView()
        currentWeatherView.hideDragSymbol()
        
        let forecastWeatherTitle = getTitleLabel(text: "날씨 예보")
        self.view.addSubview(forecastWeatherTitle)
        forecastWeatherTitle.anchor(top: currentWeatherContainer.bottomAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: UIEdgeInsets(top: 40, left: 10, bottom: 0, right: 0), size: CGSize(width: 0, height: 40))
        
        self.view.addSubview(collectionViewContainer)
        collectionViewContainer.anchor(top: forecastWeatherTitle.bottomAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        
        currentWeatherView.update(districtName: districtName, shortTermItemArr: shortTermItemArr, processedSpaceData: processedSpaceData)
        updateSpaceData(processedSpaceData: processedSpaceData)
    }
    
    func updateSpaceData(processedSpaceData: ProcessedSpaceData?) {
        if let psd = processedSpaceData {
            
            collectionData.append(psd.getToday())
            collectionData.append(psd.getAfter1())
            collectionData.append(psd.getAfter2())
            
            attachCollectionView()
        }
    }
    
    private func attachCollectionView() {
        logger("attachCollectionView------------------------------------------------")
        self.view.layoutIfNeeded()
        
        itemWidth = collectionViewContainer.bounds.size.width * 0.85
        itemHeight = collectionViewContainer.bounds.size.height * 0.8
        
        let layout = UPCarouselFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.sideItemScale = 0.8
        layout.sideItemAlpha = 1
        layout.spacingMode = .fixed(spacing: collectionViewContainer.bounds.size.width * 0.04)
        
        weatherCollectionView = UICollectionView(frame: collectionViewContainer.bounds, collectionViewLayout: layout)
        weatherCollectionView.register(WeatherCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        weatherCollectionView.dataSource = self
        weatherCollectionView.delegate = self
        weatherCollectionView.backgroundColor = .white
        weatherCollectionView.showsHorizontalScrollIndicator = false
        
        collectionViewContainer.addSubview(weatherCollectionView)
        weatherCollectionView.translatesAutoresizingMaskIntoConstraints = false
        weatherCollectionView.fillSuperView()
        
        weatherCollectionView.reloadData()
    }
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            logger("page at center = \(currentPage)")
//            logger("collectionData = \(collectionData[currentPage])")
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.weatherCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.weatherCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WeatherCollectionViewCell
        
        cell.data = self.collectionData[indexPath.row]
        cell.setUp()
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
