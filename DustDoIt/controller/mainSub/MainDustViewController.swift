//
//  MainDustViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 21..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

protocol MainDustViewDelegate {
    func setMeasureData(measureInfo: MeasureInfo)
    func clearMeasureData()
}

class MainDustViewController: UIViewController, MainDustViewDelegate {
    
    // weather
    private var weatherStackView: UIStackView!
    
    // dust
    private var contentStackView: UIStackView!
    private var topContainer: UIView!
    private var bottomLabelContainer: UIView!
    private var bottomContainer: UIView!
    private var topSquareContainer: UIView!
    private var bottomSquareContainer: UIView!
    
    private var pm10Label: CountingLabel!
    private var pm25Label: CountingLabel!
    private var pm10DescLabel: UILabel!
    private var pm25DescLabel: UILabel!
    
    private var bottomlabelFontSize: CGFloat = 10
    private var descLabelFontSize: CGFloat = 12
    private var pmLabelFontSize: CGFloat = 70
    
    private let circleWidth:CGFloat = 1
    private let circleSize: CGFloat = 0.75
    private let animDuration: CFTimeInterval = 1
    
    private var topColorShapeLayer: CAShapeLayer?
    private var bottomColorShapeLayer: CAShapeLayer?
    private var topGrayShapeLayer: CAShapeLayer?
    private var bottomGrayShapeLayer: CAShapeLayer?
    
    private var isLaidOut = false
    
    private var measureInfo: MeasureInfo? {
        didSet {
            if let measureInfo = measureInfo {
                DispatchQueue.main.async {
                    self.removeColorTrack()
                    
                    self.topContainer.layoutIfNeeded()
                    self.bottomContainer.layoutIfNeeded()
                    self.topSquareContainer.layoutIfNeeded()
                    self.bottomSquareContainer.layoutIfNeeded()
                    
                    if let time = measureInfo.dataTime, let stationName = measureInfo.stationName {
                        
//                        logger("MainContentView measureInfo stationName === \(stationName)")
                        self.bottomRightStationNameLabel.text = "\(stationName) \(str_measureStation)  \(time)"
                    }
                    if let pm10 = Int(measureInfo.pm10Value!) {
                        self.setPm10ColorTrackCircleLayer(pm10: pm10)
                    } else {
                        self.removePm10Ui()
                    }
                    
                    if let pm25 = Int(measureInfo.pm25Value!) {
                        self.setPm25ColorTrackCircleLayer(pm25: pm25)
                    } else {
                        self.removePm25Ui()
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.removePm10Ui()
                    self.removePm25Ui()
                }
            }
        }
    }
    
    private func getClearView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }
    
    private func getPmDescriptionLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: self.descLabelFontSize)
        label.textAlignment = .center
        return label
    }
    
    private func getPmValueLabel() -> CountingLabel {
        let label = CountingLabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: self.pmLabelFontSize, weight: .ultraLight)
        label.textAlignment = .center
        return label
    }
    
    private lazy var bottomRightStationNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: self.bottomlabelFontSize)
        label.textAlignment = .right
        return label
    }()
    
    private lazy var bottomLeftInfoLabel: UILabel = {
        let label = UILabel()
        let attributedText = NSMutableAttributedString(string: str_stateGood, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize), NSAttributedString.Key.foregroundColor: UIColor.myBlue])
        attributedText.append(NSMutableAttributedString(string: str_stateNormal, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize), NSAttributedString.Key.foregroundColor: UIColor.myGreen]))
        attributedText.append(NSMutableAttributedString(string: str_stateBad, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize), NSAttributedString.Key.foregroundColor: UIColor.myYellow]))
        attributedText.append(NSMutableAttributedString(string: str_stateVeryBad, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize), NSAttributedString.Key.foregroundColor: UIColor.red]))
        
        label.attributedText = attributedText
        label.textAlignment = .left
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger("viewDidLoad")
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logger("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logger("viewDidAppear")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        logger("MainContentView viewDidLayoutSubviews")
        setGrayTrackCircleLayer()
    }
    
    func setMeasureData(measureInfo: MeasureInfo) {
        self.measureInfo = measureInfo
    }
    
    func clearMeasureData() {
        self.measureInfo = nil
    }
    
    private func removePm10Ui() {
        self.pm10Label.text = "..."
        self.pm10Label.textColor = .lightGray
        self.topColorShapeLayer?.removeFromSuperlayer()
    }
    
    private func removePm25Ui() {
        self.pm25Label.text = "..."
        self.pm25Label.textColor = .lightGray
        self.bottomColorShapeLayer?.removeFromSuperlayer()
    }
    
    
    
    
    // trait fot iPhone or iPad
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        let idiomPad = UITraitCollection(userInterfaceIdiom: .pad)
        if traitCollection.containsTraits(in: idiomPad) {
            self.bottomlabelFontSize = 18
            self.descLabelFontSize = 24
            self.pmLabelFontSize = 100
        }
    }
    
    
    
    
    private func getBasicAnimation(toValue: CGFloat) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = toValue
        animation.duration = animDuration
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        return animation
    }
    
    private func getShapelayer(path: CGPath, frame: CGRect, color: CGColor) -> CAShapeLayer {
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = frame
        shapeLayer.path = path
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.lineWidth = circleWidth
        shapeLayer.strokeEnd = 0
        
        return shapeLayer
    }
    
    private func setMeasureInfoPm10TextAnimation(pm10: Int) {
        pm10Label.textColor = UIColor.getMeasuredPm10Color(source: pm10)
        pm10Label.countFrom(fromValue: 0, to: Float(pm10), withDuration: animDuration, andAnimationType: .linear, andCountingType: .int)
    }
    
    private func setMeasureInfoPm25TextAnimation(pm25: Int) {
        pm25Label.textColor = UIColor.getMeasuredPm25Color(source: pm25)
        pm25Label.countFrom(fromValue: 0, to: Float(pm25), withDuration: animDuration, andAnimationType: .linear, andCountingType: .int)
    }
    
    private func setPm10ColorTrackCircleLayer(pm10: Int) {
        
        let topCircularPath = UIBezierPath(ovalIn: topSquareContainer.bounds)
        let pm10Animation = getBasicAnimation(toValue: CGFloat(pm10) / CGFloat(pm10 > 150 ? pm10 : 150))
        
        topColorShapeLayer = getShapelayer(path: topCircularPath.cgPath, frame: self.topSquareContainer.bounds, color: UIColor.getMeasuredPm10Color(source: pm10).cgColor)
        
        self.topSquareContainer.layer.addSublayer(topColorShapeLayer!)
        
        topColorShapeLayer?.add(pm10Animation, forKey: "basic")
        setMeasureInfoPm10TextAnimation(pm10: pm10)
    }
    
    private func setPm25ColorTrackCircleLayer(pm25: Int) {
        
        let bottomCircularPath = UIBezierPath(ovalIn: bottomSquareContainer.bounds)
        let pm25Animation = getBasicAnimation(toValue: CGFloat(pm25) / CGFloat(pm25 > 75 ? pm25 : 75))
        
        bottomColorShapeLayer = getShapelayer(path: bottomCircularPath.cgPath, frame: self.bottomSquareContainer.bounds, color: UIColor.getMeasuredPm25Color(source: pm25).cgColor)
        
        self.bottomSquareContainer.layer.addSublayer(bottomColorShapeLayer!)
        
        bottomColorShapeLayer?.add(pm25Animation, forKey: "basic")
        setMeasureInfoPm25TextAnimation(pm25: pm25)
    }
    
    private func removeColorTrack() {
        self.topColorShapeLayer?.removeFromSuperlayer()
        self.bottomColorShapeLayer?.removeFromSuperlayer()
    }
    
    private func removeGrayTrack() {
        self.topGrayShapeLayer?.removeFromSuperlayer()
        self.bottomGrayShapeLayer?.removeFromSuperlayer()
    }
    
    private func setGrayTrackCircleLayer() {
        if self.isLaidOut {
            return
        }
        self.isLaidOut = true
        
        removeGrayTrack()
        
        topSquareContainer.layoutIfNeeded()
        bottomSquareContainer.layoutIfNeeded()
        
        let topCircularPath = UIBezierPath(ovalIn: topSquareContainer.bounds)
        let bottomCircularPath = UIBezierPath(ovalIn: bottomSquareContainer.bounds)
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.04).cgColor
        
        topGrayShapeLayer = CAShapeLayer()
        topGrayShapeLayer?.frame = topSquareContainer.bounds
        topGrayShapeLayer?.path = topCircularPath.cgPath
        topGrayShapeLayer?.fillColor = UIColor.clear.cgColor
        topGrayShapeLayer?.strokeColor = color
        topGrayShapeLayer?.lineCap = CAShapeLayerLineCap.round
        topGrayShapeLayer?.lineWidth = circleWidth
        
        bottomGrayShapeLayer = CAShapeLayer()
        bottomGrayShapeLayer?.frame = bottomSquareContainer.bounds
        bottomGrayShapeLayer?.path = bottomCircularPath.cgPath
        bottomGrayShapeLayer?.fillColor = UIColor.clear.cgColor
        bottomGrayShapeLayer?.strokeColor = color
        bottomGrayShapeLayer?.lineCap = CAShapeLayerLineCap.round
        bottomGrayShapeLayer?.lineWidth = circleWidth
        
        self.topSquareContainer.layer.addSublayer(topGrayShapeLayer!)
        self.bottomSquareContainer.layer.addSublayer(bottomGrayShapeLayer!)
    }
    
    private func setLayout() {
        
        self.topContainer = getClearView()
        self.bottomContainer = getClearView()
        self.topSquareContainer = getClearView()
        self.bottomSquareContainer = getClearView()
        self.bottomLabelContainer = getClearView()
        
        self.pm10Label = getPmValueLabel()
        self.pm25Label = getPmValueLabel()
        self.pm10DescLabel = getPmDescriptionLabel(text: str_dustPm10)
        self.pm25DescLabel = getPmDescriptionLabel(text: str_dustPm25)
        
        self.view.addSubview(bottomLabelContainer)

        bottomLabelContainer.translatesAutoresizingMaskIntoConstraints = false

        let bottomLabelContainerWidthConstraint = NSLayoutConstraint(item: bottomLabelContainer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.width)
        let bottomLabelContainerHeightConstraint = NSLayoutConstraint(item: bottomLabelContainer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 24)
        let bottomLabelContainerLeadingMargin = NSLayoutConstraint(item: bottomLabelContainer, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 4)
        let bottomLabelContainerTrailingMargin = NSLayoutConstraint(item: bottomLabelContainer, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -4)
        
        bottomLabelContainerWidthConstraint.priority = UILayoutPriority(999)
        bottomLabelContainerHeightConstraint.priority = UILayoutPriority(999)
        bottomLabelContainerLeadingMargin.priority = UILayoutPriority(999)
        bottomLabelContainerTrailingMargin.priority = UILayoutPriority(999)

        self.view.addConstraints([bottomLabelContainerWidthConstraint, bottomLabelContainerHeightConstraint, bottomLabelContainerLeadingMargin, bottomLabelContainerTrailingMargin])
        
        bottomLabelContainer.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        bottomLabelContainer.addSubview(bottomRightStationNameLabel)
        bottomLabelContainer.addSubview(bottomLeftInfoLabel)
        
        bottomLeftInfoLabel.fillSuperView()
        bottomRightStationNameLabel.fillSuperView()
        
        
        
        /*
         weather info area
         */
        
        
        
        contentStackView = UIStackView(arrangedSubviews: [topContainer, bottomContainer])
        contentStackView.axis = .vertical
        contentStackView.distribution = .fillEqually
        
        self.view.addSubview(contentStackView)
        
        contentStackView.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: bottomLabelContainer.topAnchor, trailing: self.view.trailingAnchor)
        
        topContainer.addSubview(topSquareContainer)
        bottomContainer.addSubview(bottomSquareContainer)
        
        let isPortrait = UIDevice.current.orientation.isPortrait
        
        topSquareContainer.widthAnchor.constraint(equalTo: isPortrait ? topContainer.heightAnchor : topContainer.widthAnchor, multiplier: circleSize).isActive = true
        topSquareContainer.heightAnchor.constraint(equalTo: topSquareContainer.widthAnchor, multiplier: 1).isActive = true
        topSquareContainer.centerXAnchor.constraint(equalTo: topContainer.centerXAnchor).isActive = true
        topSquareContainer.centerYAnchor.constraint(equalTo: topContainer.centerYAnchor).isActive = true
        
        bottomSquareContainer.widthAnchor.constraint(equalTo: isPortrait ?  bottomContainer.heightAnchor : bottomContainer.widthAnchor, multiplier: circleSize).isActive = true
        bottomSquareContainer.heightAnchor.constraint(equalTo: bottomSquareContainer.widthAnchor, multiplier: 1).isActive = true
        bottomSquareContainer.centerXAnchor.constraint(equalTo: bottomContainer.centerXAnchor).isActive = true
        bottomSquareContainer.centerYAnchor.constraint(equalTo: bottomContainer.centerYAnchor).isActive = true
        
        topSquareContainer.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi / 2)
        bottomSquareContainer.transform = CGAffineTransform.init(rotationAngle: -CGFloat.pi / 2)
        
        self.topContainer.addSubview(pm10Label)
        pm10Label.anchor(top: topSquareContainer.topAnchor, leading: topSquareContainer.leadingAnchor, bottom: topSquareContainer.bottomAnchor, trailing: topSquareContainer.trailingAnchor)
        
        self.bottomContainer.addSubview(pm25Label)
        pm25Label.anchor(top: bottomSquareContainer.topAnchor, leading: bottomSquareContainer.leadingAnchor, bottom: bottomSquareContainer.bottomAnchor, trailing: bottomSquareContainer.trailingAnchor)
        
        self.topContainer.addSubview(pm10DescLabel)
        pm10DescLabel.anchor(top: nil, leading: topSquareContainer.leadingAnchor, bottom: topSquareContainer.bottomAnchor, trailing: topSquareContainer.trailingAnchor)
        pm10DescLabel.heightAnchor.constraint(equalTo: topSquareContainer.heightAnchor, multiplier: 0.5).isActive = true
        
        self.bottomContainer.addSubview(pm25DescLabel)
        pm25DescLabel.anchor(top: nil, leading: bottomSquareContainer.leadingAnchor, bottom: bottomSquareContainer.bottomAnchor, trailing: bottomSquareContainer.trailingAnchor)
        pm25DescLabel.heightAnchor.constraint(equalTo: bottomSquareContainer.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context -> Void in
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                self.contentStackView.axis = .vertical
                logger("Portrait")
            case .landscapeLeft,.landscapeRight :
                self.contentStackView.axis = .horizontal
                logger("Landscape")
            default:
                logger("Anything But Portrait")
            }
        }, completion: { context -> Void in
            logger("viewWillTransition completion")
            //refresh view once rotation is completed not in will transition as it returns incorrect frame size. Refresh here
//            if UIDevice.current.orientation.isPortrait {
//                self.contentStackView.axis = .vertical
//            } else {
//                self.contentStackView.axis = .horizontal
//            }
            
            self.isLaidOut = false
            self.setGrayTrackCircleLayer()
            
            self.topContainer.layoutIfNeeded()
            self.bottomContainer.layoutIfNeeded()
            self.topSquareContainer.layoutIfNeeded()
            self.bottomSquareContainer.layoutIfNeeded()

            self.topColorShapeLayer?.frame = self.topSquareContainer.bounds
            self.topGrayShapeLayer?.frame = self.topSquareContainer.bounds
            self.bottomColorShapeLayer?.frame = self.bottomSquareContainer.bounds
            self.bottomGrayShapeLayer?.frame = self.bottomSquareContainer.bounds
            
            let tempMeasureInfo = self.measureInfo
            self.measureInfo = tempMeasureInfo!
        })
        super.viewWillTransition(to: size, with: coordinator)

    }
    
    
    
    
    
    
    
}
