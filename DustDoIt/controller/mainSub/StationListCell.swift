//
//  StationListCell.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 21..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class StationListCell: UICollectionViewCell {
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14, weight: .ultraLight)
        return label
    }()
    
    private var station: Station? {
        didSet {
            guard let station = station else {return}
            self.label.text = station.stationName
        }
    }
    
    func setStation(station: Station) {
        setLayout()
        self.station = station
    }
    
    private func setLayout() {
        self.backgroundColor = .clear
        self.addSubview(label)
        label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0))
    }
}
