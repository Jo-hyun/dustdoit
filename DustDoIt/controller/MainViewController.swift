//
//  MainViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 9..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit
import CoreLocation
import DeckTransition

class MainViewController: UIViewController, CLLocationManagerDelegate, UIScrollViewDelegate, UISearchBarDelegate, UISearchResultsUpdating, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private lazy var mainDustContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var stationListContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .clear

        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)

        return view
    }()
    
    private lazy var stationCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero,collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(StationListCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        return collectionView
    }()
    
    private lazy var refreshButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(#imageLiteral(resourceName: "refresh"), for: .normal)
        button.alpha = 0.3
        
        return button
    }()
    
    private lazy var weatherContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapWeatherContainer))
        view.addGestureRecognizer(tap)
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeUpWeatherContainer))
        swipeUp.direction = .up
        view.addGestureRecognizer(swipeUp)
        
        return view
    }()
    
    private lazy var weatherContainerSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        
        return view
    }()
    
    private var stationCollectionViewStates : Int = 0 {
        didSet(oldVal) {
            if oldVal != stationCollectionViewStates {
                if stationCollectionViewStates == 0 {
                    self.stationListContainer.isHidden = false
                } else {
                    self.stationListContainer.isHidden = true
                }
            }
        }
    }
    
    private let reuseIdentifier = "stationCell"
    private var stations: [Station] = [] {
        didSet {
            if stations.count == 0 {
                stationCollectionView.addGestureRecognizer(collectionViewTapRecognizer)
            } else {
                stationCollectionView.removeGestureRecognizer(collectionViewTapRecognizer)
            }
        }
    }
    
    private var currentWeatherView: CurrentWeatherView!
    private var mainDustView: MainDustViewController?
    private var mainDustViewDelegate: MainDustViewDelegate?
    
    private var locationMgr = CLLocationManager()
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var getNearStation: GetNearStation!
    private var getMeasureInfos: GetMeasureInfos!
    private var getAddress: GetAddress!
    private var getSpaceDataForecast: GetSpaceDataForecast!
    private var getShortTermForecast: GetShortTermForecast!
    
    private var nearStationName: String?
    private var collectionViewTapRecognizer: UITapGestureRecognizer!
    private var networkFailCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        collectionViewTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleEmptyCollctionViewTap))
        
        setLayout()
        setNotification()
        setGestureRecognizer()
        getLocation()
        
        self.stationCollectionViewStates = 1
    }
    
    @objc func handleEmptyCollctionViewTap() {
        self.searchController.searchBar.endEditing(true)
        self.searchController.searchBar.setShowsCancelButton(false, animated: true)
        self.stationCollectionViewStates = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
        logger("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logger("viewDidAppear")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        logger("MainViewController viewDidLayoutSubviews")
    }
    
    private func setNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc func appMovedToForeground() {
        logger("App moved to ForeGround!-------------------------")
        getLocation()
    }
    
    @objc func appMovedToBackground() {
        logger("App moved to Background!-------------------------")
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        self.queryStationToDB(name: searchText)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.stationCollectionViewStates = 0
        self.searchController.searchBar.setShowsCancelButton(true, animated: true)
        if let searchText = searchBar.text {
            self.queryStationToDB(name: searchText)
        }
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.stationCollectionViewStates = 0
        if let searchText = searchBar.text {
            self.queryStationToDB(name: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.stationCollectionViewStates = 1
    }
    
    private func getLocation() {
        self.searchController.searchBar.endEditing(true)
        LoadingIndicator.show()
        
        /*
         for simulator test begin
         */
//        let longitude = 126.674228167138
//        let latitude = 37.4487087604968
//        self.getAddress(x: longitude, y: latitude)
//
//        let wgs84_point = GeographicPoint(x: longitude, y: latitude)
//        let converter = GeoConverter()
//        let tm_point = converter.convert(sourceType: .WGS_84, destinationType: .TM, geoPoint: wgs84_point)
//
//        if let pos = tm_point {
//            self.getNearStation(tmX: pos.x, tmY: pos.y)
//        }
        /*
         for simulator test end
         */
        
        
        
        
        let status  = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            self.locationMgr.requestWhenInUseAuthorization()
            LoadingIndicator.hide()
            return
        }

        if status == .denied || status == .restricted {
            self.showLocationDisabledAlert()
            return
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.locationMgr.delegate = self
            self.locationMgr.desiredAccuracy = kCLLocationAccuracyBest
            self.locationMgr.startUpdatingLocation()
        }
    }
    
    /*
     core location
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationMgr.stopUpdatingLocation()
        
        if let currentLocation = locations.last {
            let longitude = currentLocation.coordinate.longitude
            let latitude = currentLocation.coordinate.latitude
//            logger("x = \(longitude), y = \(latitude)")
            
            self.getAddress(x: longitude, y: latitude)
            
            let wgs84_point = GeographicPoint(x: longitude, y: latitude)
            let converter = GeoConverter()
            let tm_point = converter.convert(sourceType: .WGS_84, destinationType: .TM, geoPoint: wgs84_point)

            if let pos = tm_point {
                self.getNearStation(tmX: pos.x, tmY: pos.y)
//                logger("pos.x = \(pos.x), pos.y = \(pos.y)")
            }
        } else {
            LoadingIndicator.hide()
            logger("locations.last == nil")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        logger("Error \(error)")
        LoadingIndicator.hide()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        if status == .denied || status == .restricted {
            self.showLocationDisabledAlert()
            return
        }
        getLocation()
    }
    
    private func showFailAlert(pos: String) {
        DispatchQueue.main.async {
            LoadingIndicator.hide()
            logger("통신 에러 - \(pos)")
        }
    }
    
    private func showLocationDisabledAlert() {
        let alert = UIAlertController(title: "위치 정보", message: "위치 정보 사용 허가를 위해 세팅으로 이동합니다", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "세팅", style: .default, handler: { _ in
            DispatchQueue.main.async {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: { (success) in
                        logger("Settings opened: \(success)")
                    })
                }
            }
        })
        let cancelAction = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
        LoadingIndicator.hide()
    }
    
    /*
     GestureRecognizer
     */
    private func setGestureRecognizer() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .right
        view.addGestureRecognizer(edgePan)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(screenSwiped))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(screenSwipedDown))
        swipeDown.direction = .down
        view.addGestureRecognizer(swipeDown)
    }
    
    @objc func screenSwipedDown(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .recognized {
            refreshView()
        }
    }
    
    @objc func screenSwiped(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.state == .recognized {
            self.pushDustForecastView()
        }
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.pushDustForecastView()
        }
    }
    
    @objc func handleRefreshTap() {
        refreshView()
    }
    
    @objc func tapWeatherContainer() {
        showWeatherView()
    }
    
    @objc func swipeUpWeatherContainer() {
        showWeatherView()
    }
    
    private func refreshView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.refreshButton.transform = self.refreshButton.transform.rotated(by: .pi)
        })
        getLocation()
    }
    
    private func pushDustForecastView() {
        self.navigationController?.pushViewController(DustForeCastViewController(), animated: true)
    }
    
    private func showWeatherView() {
        let modal = WeatherViewController()
        let transitionDelegate = DeckTransitioningDelegate()
        modal.transitioningDelegate = transitionDelegate
        modal.modalPresentationStyle = .custom
        modal.districtName = districtName
        modal.shortTermItemArr = shortTermItemArr
        modal.processedSpaceData = processedSpaceData
        present(modal, animated: true, completion: nil)
    }
    
    /*
     layout
     */
    private func setLayout() {
        currentWeatherView = CurrentWeatherView()
        setSearchbar()
        
        /*
         bottom weather forecast
         */
        self.view.addSubview(weatherContainer)
        weatherContainer.anchor(top: nil, leading: self.view.leadingAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, trailing: self.view.trailingAnchor, size: CGSize(width: self.view.frame.width, height: 70))
        
        weatherContainer.addSubview(currentWeatherView)
        currentWeatherView.fillSuperView()
        
        weatherContainer.addSubview(weatherContainerSeparator)
        weatherContainerSeparator.fillSuperViewAsTopSeparator()
        
        /*
         middle fine dust
         */
        self.view.addSubview(mainDustContainer)
        mainDustContainer.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: weatherContainer.topAnchor, trailing: self.view.trailingAnchor)
        
        self.mainDustView = MainDustViewController()
        self.mainDustViewDelegate = self.mainDustView!
        
        addChild(self.mainDustView!)
        self.mainDustContainer.addSubview(self.mainDustView!.view)
        self.mainDustView!.view.frame = self.mainDustContainer.bounds
        self.mainDustView!.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mainDustView!.didMove(toParent: self)
        
        self.view.addSubview(stationListContainer)
        stationListContainer.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.stationListContainer.addSubview(stationCollectionView)
        stationCollectionView.anchor(top: self.stationListContainer.topAnchor, leading: self.stationListContainer.leadingAnchor, bottom: self.stationListContainer.bottomAnchor, trailing: self.stationListContainer.trailingAnchor, padding: UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0))
        
        self.view.addSubview(refreshButton)
        refreshButton.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        refreshButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        refreshButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        refreshButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        refreshButton.addTarget(self, action: #selector(handleRefreshTap), for: .touchUpInside)
    }
    
    private func setSearchbar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        
        searchController.loadViewIfNeeded()
        
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "미세먼지 지역 검색..."
        searchController.searchBar.backgroundColor = .clear
        searchController.searchBar.barTintColor = .white
        searchController.searchBar.barStyle = .default
        searchController.searchBar.searchBarStyle = .minimal
        navigationItem.titleView = searchController.searchBar
        
        let leftButton = UIButton(type: .system)
        leftButton.setImage(UIImage(named: "info.png"), for: .normal)
        leftButton.tintColor = .black
        let leftBarButton = UIBarButtonItem(customView: leftButton)
        
        let rightButton = UIButton(type: .system)
        rightButton.setImage(UIImage(named: "forecast.png"), for: .normal)
        rightButton.tintColor = .black
        let rightBarButton = UIBarButtonItem(customView: rightButton)
        
        navigationItem.leftBarButtonItem = leftBarButton
        navigationItem.rightBarButtonItem = rightBarButton
        
        if #available(iOS 11.0, *) {
            leftButton.translatesAutoresizingMaskIntoConstraints = false
            leftButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
            leftButton.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
            
            rightButton.translatesAutoresizingMaskIntoConstraints = false
            rightButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
            rightButton.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        } else {
            var frameL = leftButton.frame
            frameL.size.width = 32.0
            frameL.size.height = 32.0
            leftButton.frame = frameL
            
            var frameR = rightButton.frame
            frameR.size.width = 32.0
            frameR.size.height = 32.0
            rightButton.frame = frameR
        }
        
        leftButton.addTarget(self, action: #selector(handleNavLeftButton), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(handleNavRigthButton), for: .touchUpInside)
    }
    
    @objc func handleNavLeftButton() {
        self.present(InfoViewController(), animated: true, completion: nil)
        self.stationCollectionViewStates = 1
    }
    
    @objc func handleNavRigthButton() {
        self.pushDustForecastView()
        self.stationCollectionViewStates = 1
    }
    
    private func queryStationToDB(name: String) {
        let queryStations = DBManager.shared.getStationsWithName(name: name)
        if let stations = queryStations {
            self.stations.removeAll()
            self.stations.append(contentsOf: stations)
            self.stationCollectionView.reloadData()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchController.searchBar.endEditing(true)
    }
    
    /*
     uicollectionview
     */
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StationListCell
        
        cell.setStation(station: self.stations[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StationListCell
        cell.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let stationName = self.stations[indexPath.row].stationName
            self.getMeasureInfo(with: stationName)
            self.searchController.searchBar.endEditing(true)
            self.stationCollectionViewStates = 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.searchController.searchBar.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func updateCurrentForecast() {
        currentWeatherView.update(districtName: districtName, shortTermItemArr: shortTermItemArr, processedSpaceData: processedSpaceData)
        logger("updateCurrentForecast-----------------------------------------------------------------------------")
    }
    
    private var districtName: [String: String] = [:]
    private var shortTermItemArr: [ShortTermItem]?
    private var processedSpaceData: ProcessedSpaceData?
}









/*
 API callback
 */

extension MainViewController: NetworkCallback {
    
    private func getNearStation(tmX: Double, tmY: Double) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getNearStation = GetNearStation(tmX: "\(tmX)", tmY: "\(tmY)")
            sf.getNearStation.setNetworkCallback(callback: sf, callbackName: .nearStation)
            sf.getNearStation.startRequest()
        }
    }
    
    private func getMeasureInfo(with stationName: String) {
        DispatchQueue.main.async {
            LoadingIndicator.show()
            self.nearStationName = stationName
        }
        
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getMeasureInfos = GetMeasureInfos(stationName: stationName)
            sf.getMeasureInfos.setNetworkCallback(callback: sf, callbackName: .measureInfos)
            sf.getMeasureInfos.startRequest()
        }
    }
    
    private func getAddress(x: Double, y: Double) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getAddress = GetAddress(x: x, y: y)
            sf.getAddress.setNetworkCallback(callback: sf, callbackName: .address)
            sf.getAddress.startRequest()
        }
    }
    
    private func getSpaceDataForecast(baseDate: String, baseTime: String, x: String, y: String) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getSpaceDataForecast = GetSpaceDataForecast(baseDate: baseDate, baseTime: baseTime, x: x, y: y)
            sf.getSpaceDataForecast.setNetworkCallback(callback: sf, callbackName: .spaceDataForecast)
            sf.getSpaceDataForecast.startRequest()
        }
    }
    
    private func getShortTermForecast(baseDate: String, baseTime: String, x: String, y: String) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getShortTermForecast = GetShortTermForecast(baseDate: baseDate, baseTime: baseTime, x: x, y: y)
            sf.getShortTermForecast.setNetworkCallback(callback: sf, callbackName: .shortTermForecast)
            sf.getShortTermForecast.startRequest()
        }
    }
    
    // TODO - api 에러코드 처리
    
    func getResult(name: NetCallBack, result: Bool) {
        switch name {
        case .nearStation:
            guard let data = getNearStation.returnData() else {return}
            guard let nearStations = data.list else {return}
            if nearStations.count == 0 {return}
            
            if let stationName = nearStations[0].stationName {
                self.getMeasureInfo(with: stationName)
            } else {
                self.showFailAlert(pos: "1")
            }
            
        case .measureInfos:
            guard let data = getMeasureInfos.returnData() else {return}
            guard let measureInfos = data.list else {return}
            if measureInfos.count == 0 {return}
            
            var measureInfo = measureInfos[0]
            if let nearStationName = self.nearStationName {
                measureInfo.stationName = nearStationName
            }
            
            self.mainDustViewDelegate?.clearMeasureData()
            self.mainDustViewDelegate?.setMeasureData(measureInfo: measureInfo)
            
        case .address:
            guard let data = getAddress.returnData() else {return}
//            guard let status = data.response?.status else {return}
            guard let result = data.response?.result else {return}
            if result.count == 0 {return}
            guard let structure = result[0].structure else {return}
            
            guard let si = structure.level1 else {return}
            guard let gu = structure.level2 else {return}
            guard let dong = structure.level4A else {return}
            guard let ddong = structure.level4L else {return}
            
            self.districtName["gu"] = gu
            self.districtName["dong"] = dong
            
            if dong == "" {
                self.districtName["dong"] = ddong
            }
            
            let baseDate = Date.shortTermBaseDateWith(format: "yyyyMMdd")
            var x = "54", y = "124" // 인천 남구 격자 좌표
            if let district = DBManager.shared.getDistrictsWith(si: si, gu: gu), district.count > 0 {
                x = district[0].x
                y = district[0].y
            }
            
            self.getShortTermForecast(baseDate: baseDate, baseTime: Date.shortTermBaseTime(), x: x, y: y)
            
            
        case .shortTermForecast:
            guard let forecast = getShortTermForecast.returnData() else {return}
            guard let response = forecast.response else {return}
            guard let body = response.body else {return}
            guard let items = body.items else {return}
            guard let item = items.item else {return}
            if item.count == 0 {return}
            
            guard let x = item[0].nx else {return}
            guard let y = item[0].ny else {return}
            
            if item.count > 0 {
                self.shortTermItemArr = item
            }
            
            let baseDate = Date.spaceDataBaseDateWith(format: "yyyyMMdd")
            
            self.getSpaceDataForecast(baseDate: "\(baseDate)", baseTime: Date.spaceDataBaseTime(), x: "\(x)", y: "\(y)")
            
            
        case .spaceDataForecast:
            guard let forecast = getSpaceDataForecast.returnData() else {return}
            guard let response = forecast.response else {return}
            guard let body = response.body else {return}
            guard let items = body.items else {return}
            guard let item = items.item else {return}
            
            let parser = MainSpaceDataParser(spaceDataItems: item)
            self.processedSpaceData = parser.getProcessedData()
            
            self.updateCurrentForecast()
            
            
        case .noInternet:
            let alert = UIAlertController(title: "네트워크", message: "인터넷 연결을 확인해 주세요", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.getLocation()
            }))
            self.present(alert, animated: true, completion: nil)
            
        case .networkFailure:
            var message: String = "서버와 통신을 완료하지 못했습니다."
            if (networkFailCount != 0 && networkFailCount % 5 == 0) {
                message = "서버가 응답이 없습니다.\n잠시후 다시 시도해 주세요."
            }
            
            let alert = UIAlertController(title: "통신장애", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.networkFailCount += 1
                self.getLocation()
            }))
            self.present(alert, animated: true, completion: nil)
        default: break
        }
        LoadingIndicator.hide()
    }
}

