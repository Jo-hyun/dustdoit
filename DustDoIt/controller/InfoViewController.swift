//
//  InfoViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 26..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    private lazy var topContainer: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "close.png"), for: .normal)
        button.tintColor = .black
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.addTarget(self, action: #selector(self.closeView), for: .touchUpInside)
        return button
    }()
    
    private lazy var tableImageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "table.png")
        view.contentMode = .scaleAspectFit
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        
        var longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressGestureHandler))
        longPress.minimumPressDuration = 0.5
        view.addGestureRecognizer(longPress)
        
        return view
    }()
    
//    private lazy var dustSourcelabel: UILabel = {
//        let label = UILabel()
//        label.numberOfLines = 0
//        label.textColor = .darkGray
//        label.font = UIFont.systemFont(ofSize: 12)
//
//        let textColor: UIColor = .darkGray
//        let underLineColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
//        let underLineStyle = NSUnderlineStyle.styleSingle.rawValue
//
//        let labelUnderLineAtributes: [NSAttributedStringKey : Any] = [
//            NSAttributedStringKey.foregroundColor: textColor,
//            NSAttributedStringKey.underlineStyle: underLineStyle,
//            NSAttributedStringKey.underlineColor: underLineColor,
//            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)
//        ]
//        let attrs2Text = "- 데이터 오류가능성: 데이터는 실시간 관측된 자료이며 측정소 현지 사정이나 데이터의 수신상태에 따라 미수신될 수 있습니다"
//        let attributedString2 = NSMutableAttributedString(string: "\n\n\(attrs2Text)", attributes: labelUnderLineAtributes)
//        let underLineAttributedString = NSMutableAttributedString(string: "- 측정 데이터 제공: 한국환경공단", attributes: labelUnderLineAtributes)
//
//        underLineAttributedString.append(attributedString2)
//        label.attributedText = underLineAttributedString
//
//        return label
//    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setLayout()
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDownGestureHandler(recognizer:)))
        swipe.direction = .down
        view.addGestureRecognizer(swipe)
    }
    
    private func getTitleLabel(text: String) -> UILabel {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 24)
        label.text = text
        return label
    }
    
    private func getSourceLabel(text: String) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .darkGray
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.text = text
        return label
    }
    
    @objc func swipeDownGestureHandler(recognizer: UISwipeGestureRecognizer){
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func longPressGestureHandler(recognizer: UILongPressGestureRecognizer){
        let imageView = recognizer.view as! UIImageView
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { _ in
            UIImageWriteToSavedPhotosAlbum(imageView.image!, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            present(alert, animated: true, completion: nil)
        } else {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setLayout() {
        view.addSubview(topContainer)
        topContainer.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: CGSize(width: view.frame.size.width, height: 40))
        
        topContainer.addSubview(closeButton)
        closeButton.anchor(top: topContainer.topAnchor, leading: topContainer.leadingAnchor, bottom: topContainer.bottomAnchor, trailing: nil, size: CGSize(width: 40, height: 40))
        
        let dustTitleLabel = getTitleLabel(text: "미세먼지 예보 제공")
        view.addSubview(dustTitleLabel)
        dustTitleLabel.anchor(top: topContainer.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5))
        dustTitleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        view.addSubview(tableImageView)
        tableImageView.anchor(top: dustTitleLabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5))
        tableImageView.heightAnchor.constraint(equalTo: tableImageView.widthAnchor, multiplier: 542 / 3214).isActive = true
        
        let dustSourceLabel = getSourceLabel(text: "- 측정 데이터 제공: 한국환경공단\n\n- 데이터 오류가능성: 데이터는 실시간 관측된 자료이며 측정소 현지 사정이나 데이터의 수신상태에 따라 미수신될 수 있습니다")
        view.addSubview(dustSourceLabel)
        dustSourceLabel.anchor(top: tableImageView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 15, left: 5, bottom: 0, right: 5))
        
        let temperatureTitleLabel = getTitleLabel(text: "날씨 예보 제공")
        view.addSubview(temperatureTitleLabel)
        temperatureTitleLabel.anchor(top: dustSourceLabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 30, left: 5, bottom: 0, right: 5))
        temperatureTitleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let temperatureSourceLabel = getSourceLabel(text: "- 측정 데이터 제공: 기상청")
        view.addSubview(temperatureSourceLabel)
        temperatureSourceLabel.anchor(top: temperatureTitleLabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 15, left: 5, bottom: 0, right: 5))
        
        let addressTitlelabel = getTitleLabel(text: "주소 정보 제공")
        view.addSubview(addressTitlelabel)
        addressTitlelabel.anchor(top: temperatureSourceLabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 30, left: 5, bottom: 0, right: 5))
        addressTitlelabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let addressSourcelabel = getSourceLabel(text: "- 주소 정보: vWorld")
        view.addSubview(addressSourcelabel)
        addressSourcelabel.anchor(top: addressTitlelabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: UIEdgeInsets(top: 15, left: 5, bottom: 0, right: 5))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func image(image: UIImage!, didFinishSavingWithError error: NSError!, contextInfo: AnyObject!) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "이미지 저장 에러", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "저장 완료!", message: "이미지가 앨범에 저장되었습니다", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
}
