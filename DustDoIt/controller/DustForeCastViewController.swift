//
//  DustForeCastViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 24..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class DustForeCastViewController: UIViewController, NetworkCallback {
    
    private lazy var informOveralltextView: UITextView = {
        let view = UITextView()
        view.backgroundColor = .clear
        view.textColor = .gray
        view.font = UIFont.systemFont(ofSize: 14)
        view.isScrollEnabled = false
        
        return view
    }()
    
    private var foreCastCollectionViewController: ForeCastCollectionViewController!
    private var foreCastDataSettable: ForeCastDataSettable?
    private var segmentedControl: UISegmentedControl!
    private var getPm10Forecasts: GetForecastInfo!
    private var getPm25Forecasts: GetForecastInfo!
    private var pm10Forecasts: [DustForeCast] = []
    private var pm25Forecasts: [DustForeCast] = []
    private var networkFailCount: Int = 0
    
    // if set false call yesterday -> there is no forecast between 0 and 5 oclock
    private var needTodayForecast: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setBackButton()
        
        let format = "yyyy-MM-dd"
        segmentedControl = UISegmentedControl(items: [Date.yesterdayDateWith(format: format), Date.todayDateWith(format: format), Date.tomorrowDateWith(format: format)])
        segmentedControl.sizeToFit()
        segmentedControl.selectedSegmentIndex = 0
        self.navigationItem.titleView = segmentedControl
        self.navigationController?.navigationBar.tintColor = .black
        
        setLayout()
        setTapRecognizer()
        getForeCastList(type: "PM10", today: needTodayForecast)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setBackButton() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let leftButton = UIButton(type: .system)
        leftButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        leftButton.addTarget(self, action: #selector(self.handleBack), for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftButton)
        
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        if #available(iOS 11.0, *) {
            leftButton.translatesAutoresizingMaskIntoConstraints = false
            leftButton.widthAnchor.constraint(equalToConstant: 32.0).isActive = true
            leftButton.heightAnchor.constraint(equalToConstant: 32.0).isActive = true
        } else {
            var frameL = leftButton.frame
            frameL.size.width = 32.0
            frameL.size.height = 32.0
            leftButton.frame = frameL
        }
    }
    
    @objc func handleBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setSegmentedController(title: [String]) {
        segmentedControl.removeAllSegments()
        segmentedControl.insertSegment(withTitle: "\(title[0])", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "\(title[1])", at: 1, animated: false)
        segmentedControl.insertSegment(withTitle: "\(title[2])", at: 2, animated: false)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        
        updateView(idx: segmentedControl.selectedSegmentIndex)
    }
    
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        updateView(idx: sender.selectedSegmentIndex)
    }
    
    private func updateView(idx: Int) {
        let pm10Forecasts = self.pm10Forecasts[idx]
        
        foreCastDataSettable?.setForecast(forecast: pm10Forecasts)
        
        if let informOverall = pm10Forecasts.informOverall {
            self.informOveralltextView.text = informOverall.trimmingCharacters(in: .newlines)
            self.informOveralltextView.sizeToFit()
        }
    }
    
    func getResult(name: NetCallBack, result: Bool) {
        switch name {
        case .pm10Forecasts:
            if let forecasts = self.getPm10Forecasts.returnData()?.list {
                if forecasts.count < 3 {
                    self.needTodayForecast = false
                    getForeCastList(type: "PM10", today: needTodayForecast)
                } else {
                    self.pm10Forecasts.removeAll()
                    for i in 0...2 {
                        self.pm10Forecasts.append(forecasts[i])
                    }
//                        getForeCastList(type: "PM25", today: needTodayForecast) // 초미세먼지 보여줄때 사용
                    self.setSegmentedController(title: [self.pm10Forecasts[0].informData!, self.pm10Forecasts[1].informData!, self.pm10Forecasts[2].informData!])
                    
                    if let informOverall = self.pm10Forecasts[0].informOverall {
                        self.informOveralltextView.text = informOverall
                    } else {
                        self.informOveralltextView.text = "예보 없음.."
                    }
                }
            } else {
                let alert = UIAlertController(title: "통신 에러", message: "통신을 완료하지 못했습니다. 다시 불러오시겠습니까?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "아니요", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "네", style: .default, handler: { _ in
                    self.getForeCastList(type: "PM10", today: self.needTodayForecast)
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
        case .pm25Forecasts:
            if let forecasts = self.getPm25Forecasts.returnData()?.list {
                self.pm25Forecasts.removeAll()
                for i in 0...2 {
                    self.pm25Forecasts.append(forecasts[i])
                }
                
                self.setSegmentedController(title: [self.pm10Forecasts[0].informData!, self.pm10Forecasts[1].informData!, self.pm10Forecasts[2].informData!])
                
                if let informOverall = self.pm10Forecasts[0].informOverall {
                    self.informOveralltextView.text = informOverall
                }
            }
            
        case .noInternet:
            let alert = UIAlertController(title: "네트워크", message: "인터넷 연결을 확인해 주세요", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.getForeCastList(type: "PM10", today: self.needTodayForecast)
            }))
            self.present(alert, animated: true, completion: nil)
            
        case .networkFailure:
            var message: String = "서버와 통신을 완료하지 못했습니다."
            if (networkFailCount != 0 && networkFailCount % 5 == 0) {
                message = "서버가 응답이 없습니다.\n잠시후 다시 시도해 주세요."
            }
            
            let alert = UIAlertController(title: "통신장애", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.networkFailCount += 1
                self.getForeCastList(type: "PM10", today: self.needTodayForecast)
            }))
            self.present(alert, animated: true, completion: nil)
            
        default: break
        }
        LoadingIndicator.hide()
    }
    
    private func getForeCastList(type: String, today: Bool) {
        LoadingIndicator.show()
        if type == "PM10" {
            getPm10Forecasts = GetForecastInfo(type: type, today: today)
            getPm10Forecasts.setNetworkCallback(callback: self, callbackName: .pm10Forecasts)
            getPm10Forecasts.startRequest()
        } else if type == "PM25" {
            getPm25Forecasts = GetForecastInfo(type: type, today: today)
            getPm25Forecasts.setNetworkCallback(callback: self, callbackName: .pm25Forecasts)
            getPm25Forecasts.startRequest()
        }
    }
    
    private func setTapRecognizer() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
        
        let swipePan = UISwipeGestureRecognizer(target: self, action: #selector(screenSwiped))
        view.addGestureRecognizer(swipePan)
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func screenSwiped(_ recognizer: UISwipeGestureRecognizer) {
        if recognizer.direction == .right {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func setLayout() {
        self.view.addSubview(informOveralltextView)
        informOveralltextView.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: self.view.trailingAnchor, padding: UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5))
        informOveralltextView.sizeToFit()
        
        let layout = UICollectionViewFlowLayout()
        foreCastCollectionViewController = ForeCastCollectionViewController(collectionViewLayout: layout)
        self.foreCastDataSettable = foreCastCollectionViewController
        
        addChild(foreCastCollectionViewController)
        self.view.addSubview(foreCastCollectionViewController.view)
        foreCastCollectionViewController.view.frame = view.bounds
        foreCastCollectionViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        foreCastCollectionViewController.view.anchor(top: self.informOveralltextView.bottomAnchor, leading: self.view.leadingAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, trailing: self.view.trailingAnchor)
    }
    
    private func showErrorAlert(title: String, msg: String) {
        LoadingIndicator.hide()
        logger("\(title) - \(msg)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
