//
//  CurrentWeatherView.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 13..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class CurrentWeatherView: UIView {
    
    private lazy var weatherContainerDragSymbol: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1)
        
        return view
    }()
    
    private lazy var weatherMainTemperatureLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 56, weight: .ultraLight)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = ""
        
        return label
    }()
    
    private lazy var weatherRightTopLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: .ultraLight)
        label.textColor = .lightGray
        label.textAlignment = .right
        label.text = ""
        
        return label
    }()
    
    private lazy var bottomWindImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var bottomHumidityImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private lazy var bottomCloudImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private var windLabel: UILabel!
    private var cloudLabel: UILabel!
    private var humidityLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getBottomDecriptionLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: .ultraLight)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = ""
        
        return label
    }
    
    private func setupLayout() {
        
        self.addSubview(weatherContainerDragSymbol)
        weatherContainerDragSymbol.topAnchor.constraint(equalTo: self.topAnchor, constant: 6).isActive = true
        weatherContainerDragSymbol.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        weatherContainerDragSymbol.heightAnchor.constraint(equalToConstant: 4).isActive = true
        weatherContainerDragSymbol.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1).isActive = true
        weatherContainerDragSymbol.layer.cornerRadius = 2
        weatherContainerDragSymbol.layer.masksToBounds = true
        
        self.addSubview(weatherMainTemperatureLabel)
        weatherMainTemperatureLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        weatherMainTemperatureLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        weatherMainTemperatureLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        weatherMainTemperatureLabel.widthAnchor.constraint(equalTo: weatherMainTemperatureLabel.heightAnchor, multiplier: 1.7).isActive = true
        
        self.addSubview(weatherRightTopLabel)
        weatherRightTopLabel.leadingAnchor.constraint(equalTo: weatherMainTemperatureLabel.trailingAnchor).isActive = true
        weatherRightTopLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        weatherRightTopLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        weatherRightTopLabel.bottomAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        windLabel = getBottomDecriptionLabel()
        self.addSubview(windLabel)
        windLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        windLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        windLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.addSubview(bottomWindImageView)
        bottomWindImageView.topAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        bottomWindImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomWindImageView.trailingAnchor.constraint(equalTo: windLabel.leadingAnchor, constant: -3).isActive = true
        bottomWindImageView.widthAnchor.constraint(equalTo: bottomWindImageView.heightAnchor, multiplier: 1).isActive = true
        
        humidityLabel = getBottomDecriptionLabel()
        self.addSubview(humidityLabel)
        humidityLabel.trailingAnchor.constraint(equalTo: bottomWindImageView.leadingAnchor, constant: -15).isActive = true
        humidityLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        humidityLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.addSubview(bottomHumidityImageView)
        bottomHumidityImageView.topAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        bottomHumidityImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomHumidityImageView.trailingAnchor.constraint(equalTo: humidityLabel.leadingAnchor, constant: 3).isActive = true
        bottomHumidityImageView.widthAnchor.constraint(equalTo: bottomWindImageView.heightAnchor, multiplier: 1).isActive = true
        
        cloudLabel = getBottomDecriptionLabel()
        self.addSubview(cloudLabel)
        cloudLabel.trailingAnchor.constraint(equalTo: bottomHumidityImageView.leadingAnchor, constant: -15).isActive = true
        cloudLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        cloudLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.addSubview(bottomCloudImageView)
        bottomCloudImageView.topAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        bottomCloudImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomCloudImageView.trailingAnchor.constraint(equalTo: cloudLabel.leadingAnchor, constant: -3).isActive = true
        bottomCloudImageView.widthAnchor.constraint(equalTo: bottomWindImageView.heightAnchor, multiplier: 1).isActive = true
    }
    
    func update(districtName: [String : String], shortTermItemArr: [ShortTermItem]?, processedSpaceData: ProcessedSpaceData?) {
        DispatchQueue.main.async { [weak self] in
            guard let sf = self else {return}
            var temperature: String = ""
            var time: String = ""
            var wind: String = "0 m/s"
            var skyValue: Int = 0
            var ptyValue: Int = 0
            var oneHourRainValue: Int = 0
            var humidityValue: Int = 0
            
            var rightTopText = ""
            
            if let gu = districtName["gu"] {
                rightTopText += gu
            }
            
            if let dong = districtName["dong"] {
                rightTopText += "  \(dong)"
            }
            
            if let shortItmes = shortTermItemArr {
                logger("shortItmes = \(shortItmes)")
                for item in shortItmes {
                    if let category = item.category {
                        if category == "T1H" {
                            if let t = item.obsrValue {
                                temperature = "\(Int(t))"
                            }
                        } else if category == "SKY" {
                            if let s = item.obsrValue {
                                skyValue = Int(s)
                            }
                        } else if category == "WSD" {
                            if let w = item.obsrValue {
                                wind = String(format: "%.1f", arguments: [w])
                            }
                        } else if category == "PTY" {
                            if let p = item.obsrValue {
                                ptyValue = Int(p)
                            }
                        } else if category == "RN1" {
                            if let r = item.obsrValue {
                                oneHourRainValue = Int(r)
                            }
                        } else if category == "REH" {
                            if let h = item.obsrValue {
                                humidityValue = Int(h)
                            }
                        }
                    }
                }
                
                if let baseTime = shortItmes[0].baseTime as? Int {
                    time = "\(baseTime / 100):00"
                } else if let baseTime = shortItmes[0].baseTime as? String {
                    time = "\(baseTime.prefix(2)):00"
                }
                
                rightTopText += "  \(time)"
            }
            
            sf.weatherMainTemperatureLabel.text = "\(temperature)˚"
            sf.weatherRightTopLabel.text = rightTopText
            
            sf.windLabel.text = "\(wind)m/s"
            sf.humidityLabel.text = "\(humidityValue)%"
            sf.cloudLabel.text = sf.getRainString(val: oneHourRainValue)
            
            sf.bottomWindImageView.image = #imageLiteral(resourceName: "wind")
            sf.bottomHumidityImageView.image = sf.getHumidityImage(val: humidityValue)
            sf.bottomCloudImageView.image = ptyValue == 0 ? sf.getSkyImage(val: skyValue) : sf.getPtyImage(val: ptyValue)
        }
    }
    
    func hideDragSymbol() {
        weatherContainerDragSymbol.isHidden = true
    }
    
    private func getRainString(val: Int) -> String {
        var str = "0mm"
        
        switch val {
        case 1:
            str = "1mm 미만"
        case 5:
            str = "1~4mm"
        case 10:
            str = "5~9mm"
        case 20:
            str = "10~19mm"
        case 40:
            str = "20~39mm"
        case 70:
            str = "40~69mm"
        case 100:
            str = "70mm 이상"
        default:
            str = "0mm"
        }
        
        return str
    }
    
    private func getSkyImage(val: Int) -> UIImage {
        var img: UIImage = #imageLiteral(resourceName: "weather_sun")
        
        switch val {
        case 1:
            img = #imageLiteral(resourceName: "weather_sun")
        case 2:
            img = #imageLiteral(resourceName: "weather_cloud1")
        case 3:
            img = #imageLiteral(resourceName: "weather_cloud2")
        case 4:
            img = #imageLiteral(resourceName: "weather_cloud3")
        default:
            img = #imageLiteral(resourceName: "weather_sun")
        }
        
        return img
    }
    
    private func getPtyImage(val: Int) -> UIImage {
        var img: UIImage = #imageLiteral(resourceName: "weather_rain")
        
        switch val {
        case 1:
            img = #imageLiteral(resourceName: "weather_rain")
        case 2:
            img = #imageLiteral(resourceName: "weather_rainSnow")
        case 3:
            img = #imageLiteral(resourceName: "weather_snow")
        default:
            img = #imageLiteral(resourceName: "weather_rain")
        }
        
        return img
    }
    
    private func getHumidityImage(val: Int) -> UIImage {
        var img: UIImage = #imageLiteral(resourceName: "humidity_0")
        
        switch round(Double(val / 10)) {
        case 1:
            img = #imageLiteral(resourceName: "humidity_10")
        case 2:
            img = #imageLiteral(resourceName: "humidity_20")
        case 3:
            img = #imageLiteral(resourceName: "humidity_30")
        case 4:
            img = #imageLiteral(resourceName: "humidity_40")
        case 5:
            img = #imageLiteral(resourceName: "humidity_50")
        case 6:
            img = #imageLiteral(resourceName: "humidity_60")
        case 7:
            img = #imageLiteral(resourceName: "humidity_70")
        case 8:
            img = #imageLiteral(resourceName: "humidity_80")
        case 9:
            img = #imageLiteral(resourceName: "humidity_90")
        case 10:
            img = #imageLiteral(resourceName: "humidity_100")
        default:
            img = #imageLiteral(resourceName: "humidity_50")
        }
        return img
    }
}









