//
//  ForeCastCollectionViewController.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 24..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

protocol ForeCastDataSettable {
    func setForecast(forecast: DustForeCast)
}

private let reuseIdentifier = "forecastCell"

class ForeCastCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, ForeCastDataSettable {
    
    private var forecast: DustForeCast!
    private var gradeDic: [[String : String]] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        logger("viewDidLoad")
        collectionView?.backgroundColor = .clear
        collectionView?.register(ForeCastListCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logger("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logger("viewDidAppear")
    }
    
    func setForecast(forecast: DustForeCast) {
        self.forecast = forecast
        
        if let informGrade = forecast.informGrade {
            self.gradeDic.removeAll()
            collectionView?.reloadData()
            
            let subString = informGrade.split(separator: ",")
            for string in subString {
                let str = String(string)
                let strSubString = str.split(separator: ":")
                let dic = [String(strSubString[0]) : String(strSubString[1])]
                self.gradeDic.append(dic)
            }
            collectionView?.reloadData()
        }
        logger("self.gradeDic = \(self.gradeDic)")
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gradeDic.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ForeCastListCell
        cell.setGrade(grade: self.gradeDic[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 35)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        logger("\(UIDevice.current.orientation.isLandscape)")
        collectionView?.collectionViewLayout.invalidateLayout()
    }
}
