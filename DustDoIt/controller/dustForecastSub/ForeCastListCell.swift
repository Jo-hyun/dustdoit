//
//  ForeCastListCell.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 24..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit

class ForeCastListCell: UICollectionViewCell {
    lazy var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 14, weight: .ultraLight)
        return label
    }()
    
    func setGrade(grade: [String : String]) {
        setLayout()
        
        if let first = grade.first {
            let attributedText = NSMutableAttributedString(string: "\(first.key)  -  ", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.gray])
            attributedText.append(NSMutableAttributedString(string: "\(first.value)", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: getGradeColor(grade: first.value)]))
            
            self.label.attributedText = attributedText
        }
    }
    
    func setLayout() {
        self.backgroundColor = .clear
        self.addSubview(label)
        label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: nil, padding: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
    }
    
    func getGradeColor(grade: String) -> UIColor {
        switch grade.trimmingCharacters(in: .whitespacesAndNewlines) {
        case "좋음":
            return UIColor.getMeasuredPm10Color(source: 1)
        case "보통":
            return UIColor.getMeasuredPm10Color(source: 31)
        case "나쁨":
            return UIColor.getMeasuredPm10Color(source: 81)
        case "매우나쁨":
            return UIColor.getMeasuredPm10Color(source: 151)
        default:
            return UIColor.getMeasuredPm10Color(source: -1)
        }
    }
}
