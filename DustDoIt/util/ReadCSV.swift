//
//  ReadCSV.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 17..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import CSV

class ReadCSV {
    static let shared = ReadCSV()
    let key: String = "saveAdminDistrict"
    
    private init() {}
    
    func read(filePath: String, completion: @escaping () -> Void) {
        
        if UserDefaults.standard.bool(forKey: key) {
            completion()
        } else {
            if let stream = InputStream(fileAtPath: filePath) {
                let csv = try! CSVReader(stream: stream, hasHeaderRow: true)
                var tempArr: [AdminDistrict] = []
                let group = DispatchGroup()
                
                group.enter()
                DispatchQueue.global(qos: .default).async {
                    while let row = csv.next() {
                        let adminDistrict = AdminDistrict(values: row)
                        tempArr.append(adminDistrict)
                    }
                    group.leave()
                }
                group.wait()
                
                DBManager.shared.insertData(districts: tempArr)
                UserDefaults.standard.set(true, forKey: key)
                
                completion()
            }
        }
    }
}
