//
//  Logger.swift
//  DustDoIt
//
//  Created by JH on 2018. 3. 11..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation

public func logger(fileName: String = #file, line: Int = #line, _ msg: Any) {
    #if DEBUG
        let name = (fileName as NSString).lastPathComponent
        print("\(name) - #.\(line) \n\(msg)\n")
    #endif
}
