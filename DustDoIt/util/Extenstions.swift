//
//  Extenstions.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 7..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func fillSuperView() {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor)
    }
    
    func fillSuperViewSafeArea() {
        anchor(top: superview?.safeAreaLayoutGuide.topAnchor, leading: superview?.safeAreaLayoutGuide.leadingAnchor, bottom: superview?.safeAreaLayoutGuide.bottomAnchor, trailing: superview?.safeAreaLayoutGuide.trailingAnchor)
    }
    
    func fillSuperViewAsTopSeparator() {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: nil, trailing: superview?.trailingAnchor, size: CGSize(width: (superview?.frame.width)!, height: 0.5))
    }
    
    func fillSuperViewAsBottomSeparator() {
        anchor(top: nil, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor, size: CGSize(width: (superview?.frame.width)!, height: 0.5))
    }
    
    func anchorSize(to view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        self.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            self.leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            self.trailingAnchor.constraint(equalTo: trailing, constant: -padding.right).isActive = true
        }
        
        if size.width != 0 {
            self.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            self.heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
    }
}

extension UIColor {
    static var myBlue = UIColor(red: 0, green: 135/255, blue: 255/255, alpha: 1)
    static var myGreen = UIColor(red: 0, green: 210/255, blue: 40/255, alpha: 1)
    static var myYellow = UIColor(red: 1, green: 150/255, blue: 0, alpha: 1)
    
    static func getMeasuredPm10Color(source: Int) -> UIColor {
        var color: UIColor = .lightGray
        
        if source >= 0 && source <= 30 { color = myBlue }
        else if source >= 31 && source <= 80 { color = myGreen }
        else if source >= 81 && source <= 150 { color = myYellow }
        else if source > 150 { color = .red }
        
        return color
    }
    
    static func getMeasuredPm25Color(source: Int) -> UIColor {
        var color: UIColor = .lightGray
        
        if source <= 15 { color = myBlue }
        else if source >= 16 && source <= 35 { color = myGreen }
        else if source >= 36 && source <= 75 { color = myYellow }
        else if source > 76 { color = .red }
        
        return color
    }
}

extension Date { // +86400 = one day to seconds
    static func getTodayDay() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return  calendar.component(.day, from: date)
    }
    // 내일
    static func getTomorrowDay() -> Int {
        let date = Date(timeIntervalSinceNow: 86400)
        let calendar = Calendar.current
        return  calendar.component(.day, from: date)
    }
    // 모레
    static func getTomorrowAfterOneDay() -> Int {
        let date = Date(timeIntervalSinceNow: 86400 * 2)
        let calendar = Calendar.current
        return  calendar.component(.day, from: date)
    }
    
    static func getHours() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return  calendar.component(.hour, from: date)
    }
    
    static func getMinutes() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return  calendar.component(.minute, from: date)
    }
    
    static func getSeconds() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return  calendar.component(.second, from: date)
    }
    
    static func shortTermBaseTime() -> String {
        var val: TimeInterval = 0
        if self.getMinutes() < 40 { // api 제공 시간이 매 시각 40분 이후이기 때문에 몇분인치 체크하여 40분 이하이면 한시간 전 정보 호출
            val = -3600
        }
        
        let dateformatter = DateFormatter()
        let date = Date(timeIntervalSinceNow: val)
        dateformatter.dateFormat = "HH00"
        return  dateformatter.string(from: date)
    }
    
    static func shortTermBaseDateWith(format: String) -> String {
        if self.getHours() == 0 {
            if self.getMinutes() > 40 {
                return self.todayDateWith(format: format)
            } else {
                return self.yesterdayDateWith(format: format)
            }
        } else {
            return self.todayDateWith(format: format)
        }
    }
    
    static func spaceDataBaseDateWith(format: String) -> String {
//        if self.getHours() <= 2 {
//            if self.getHours() == 2 && self.getMinutes() > 10 { // api 제공 시간이 정해진 시각 10분 이후이기 때문에 몇분인치 체크
//                return self.todayDateWith(format: format)
//            } else {
//                return self.yesterdayDateWith(format: format)
//            }
//        } else {
//            return self.todayDateWith(format: format)
//        }
        if self.getHours() <= 17 {
            if self.getHours() == 17 && self.getMinutes() > 10 {
                return self.todayDateWith(format: format)
            } else {
                return self.yesterdayDateWith(format: format)
            }
        } else {
            return self.todayDateWith(format: format)
        }
    }
    
    // 동네예보 0200, 0500, 0800, 1100, 1400, 1700, 2000, 2300 (1일 8회)
    static func spaceDataBaseTime() -> String {
        var str = "2000"
        
//        switch self.getHours() {
//        case 0, 1:
//            str = "2300"
//        case 2:
//            if self.getMinutes() > 10 {
//                str = "0200"
//            } else {
//                str = "2300"
//            }
        
        switch self.getHours() {
        case 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16:
            str = "2000" // 어제 20시
        case 17:
            if self.getMinutes() > 10 {
                str = "0500" // 오늘 17시
            } else {
                str = "2000" // 어제 20시
            }
        case 18, 19:
            str = "0500"
        case 20, 21, 22, 23:
            str = "0500"
//        case 3, 4, 5:
//            str = "0200"
//        case 6, 7, 8:
//            str = "0500"
//        case 9, 10, 11:
//            str = "0800"
//        case 12, 13, 14:
//            str = "1100"
//        case 15, 16, 17:
//            str = "1400"
//        case 18, 19, 20:
//            str = "1700"
//        case 21, 22, 23:
//            str = "2000"
        default:
            str = "2000"
        }
        
        return str
    }
    
    static func todayDateWith(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return  dateformatter.string(from: Date())
    }
    
    static func yesterdayDateWith(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return  dateformatter.string(from: Date(timeIntervalSinceNow: -86400)) // 1 day = 60sec * 60min * 24hour = 86400 sec
    }
    
    static func tomorrowDateWith(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return  dateformatter.string(from: Date(timeIntervalSinceNow: +86400)) // 1 day = 60sec * 60min * 24hour = 86400 sec
    }
}

extension String {
    
    static func getWeatherForecastTime(time: Double) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd hh:mm"
        let date = Date(timeIntervalSince1970: time)
        return dateformatter.string(from: date)
    }
    
    static func getWeatherForecastHour(time: Double) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "hh:mm"
        let date = Date(timeIntervalSince1970: time)
        dateformatter.dateFormat = "h a"
        return dateformatter.string(from: date)
    }
}











