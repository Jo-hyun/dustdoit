//
//  ShortTermForecast.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 19..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct ShortTermForecast: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        response <- map["response"]
    }
    
    var response: ShortTermResponse?
}

struct ShortTermResponse: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        header <- map["header"]
        body <- map["body"]
    }
    
    var header: ShortTermHeader?
    var body: ShortTermBody?
}

struct ShortTermHeader: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        resultCode <- map["resultCode"]
        resultMsg <- map["resultMsg"]
    }
    
    var resultCode: String?
    var resultMsg: String?
}

struct ShortTermBody: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        items <- map["items"]
        numOfRows <- map["numOfRows"]
        pageNo <- map["pageNo"]
        totalCount <- map["totalCount"]
    }
    
    var items: ShortTermItems?
    var numOfRows: Int?
    var pageNo: Int?
    var totalCount: Int?
}

struct ShortTermItems: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        item <- map["item"]
    }
    
    var item: [ShortTermItem]?
}

struct ShortTermItem: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        baseDate <- map["baseDate"]
        baseTime <- map["baseTime"]
        category <- map["category"]
        obsrValue <- map["obsrValue"]
        nx <- map["nx"]
        ny <- map["ny"]
    }
    
    var baseDate: Int?
    var baseTime: Any?
    var category: String?
    var obsrValue: Double?
    var nx: Int?
    var ny: Int?
}
