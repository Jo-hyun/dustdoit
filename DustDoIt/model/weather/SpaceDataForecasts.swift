//
//  SpaceDataForecasts.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 17..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct SpaceDataForecasts: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        response <- map["response"]
    }
    
    var response: SpaceDataResponse?
}

struct SpaceDataResponse: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        header <- map["header"]
        body <- map["body"]
    }
    
    var header: Header?
    var body: Body?
}

struct Header: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        resultCode <- map["resultCode"]
        resultMsg <- map["resultMsg"]
    }
    
    var resultCode: String?
    var resultMsg: String?
}

struct Body: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        items <- map["items"]
        numOfRows <- map["numOfRows"]
        pageNo <- map["pageNo"]
        totalCount <- map["totalCount"]
    }
    
    var items: Items?
    var numOfRows: Int?
    var pageNo: Int?
    var totalCount: Int?
}

struct Items: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        item <- map["item"]
    }
    
    var item: [SpaceDataItem]?
}

struct SpaceDataItem: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        baseDate <- map["baseDate"]
        baseTime <- map["baseTime"]
        category <- map["category"]
        fcstDate <- map["fcstDate"]
        fcstTime <- map["fcstTime"]
        fcstValue <- map["fcstValue"]
        nx <- map["nx"]
        ny <- map["ny"]
    }
    
    var baseDate: Int?
    var baseTime: Int?
    var category: String?
    var fcstDate: Int?
    var fcstTime: Any?
    var fcstValue: Int?
    var nx: Int?
    var ny: Int?
}






