//
//  Station.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 25..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Station: Object, Mappable {
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        stationName <- map["stationName"]
        addr <- map["addr"]
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    @objc dynamic var stationName = ""
    @objc dynamic var addr = ""
    @objc dynamic var id = 0
    
    func incrementID() -> Int {
        return (DBManager.shared.getAllStations()!.max(ofProperty: "id") as Int? ?? 0) + 1
    }
}

struct StationList: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        totalCount <- map["totalCount"]
        list <- map["list"]
    }
    
    var totalCount: Int?
    var list: [Station]?
}
