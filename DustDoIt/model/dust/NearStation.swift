//
//  NearStation.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 25..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct NearStations: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        list <- map["list"]
    }
    
    var list: [NearStation]?
}

struct NearStation: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        stationName <- map["stationName"]
        addr <- map["addr"]
        tm <- map["tm"]
    }
    
    var stationName: String?
    var addr: String?
    var tm: String?
}
