//
//  MeasureInfo.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 25..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct MeasureInfos: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        list <- map["list"]
    }
    
    var list: [MeasureInfo]?
}

struct MeasureInfo: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        dataTime <- map["dataTime"]
        pm10Value <- map["pm10Value"]
        pm10Value24 <- map["pm10Value24"]
        pm25Value <- map["pm25Value"]
        pm25Value24 <- map["pm25Value24"]
    }
    
    var dataTime: String?
    var pm10Value: String?
    var pm10Value24: String?
    var pm25Value: String?
    var pm25Value24: String?
    var stationName: String?
}
