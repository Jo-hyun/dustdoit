//
//  ForeCast.swift
//  DustDoIt
//
//  Created by JH on 2018. 2. 23..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct DustForeCasts: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        list <- map["list"]
    }
    
    var list: [DustForeCast]?
}

struct DustForeCast: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        dataTime <- map["dataTime"]
        informCause <- map["informCause"]
        informCode <- map["informCode"]
        informGrade <- map["informGrade"]
        informOverall <- map["informOverall"]
        informData <- map["informData"]
    }
    
    var dataTime: String?
    var informCause: String?
    var informCode: String?
    var informGrade: String?
    var informOverall: String?
    var informData: String?
}
