//
//  AdminDistrict.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 17..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import RealmSwift

class AdminDistrict: Object {
    @objc dynamic var code: String = ""
    @objc dynamic var level1: String = ""
    @objc dynamic var level2: String = ""
    @objc dynamic var level3: String = ""
    @objc dynamic var x: String = ""
    @objc dynamic var y: String = ""
    
    convenience init(values: [String]) {
        self.init()
        self.code = values[0]
        self.level1 = values[1]
        self.level2 = values[2]
        self.level3 = values[3]
        self.x = values[4]
        self.y = values[5]
    }
    
    override class func primaryKey() -> String {
        return "code"
    }
}
