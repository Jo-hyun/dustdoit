//
//  Adress.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 17..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation
import ObjectMapper

struct Address: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        response <- map["response"]
    }
    
    var response: AddressResponse?
}

struct AddressResponse: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        service <- map["service"]
        status <- map["status"]
        result <- map["result"]
    }
    
    var service: AddressService?
    var status: String?
    var result: [AddressResult]?
}

struct AddressService: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        version <- map["version"]
        operation <- map["operation"]
        time <- map["time"]
    }
    
    var name: String?
    var version: String?
    var operation: String?
    var time: String?
}

struct AddressResult: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        zipcode <- map["zipcode"]
        type <- map["type"]
        text <- map["text"]
        structure <- map["structure"]
    }
    
    var zipcode: Int?
    var type: String?
    var text: String?
    var structure: AddressResultItemStructure?
}

struct AddressResultItemStructure: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        level0 <- map["level0"]
        level1 <- map["level1"]
        level2 <- map["level2"]
        level3 <- map["level3"]
        level4L <- map["level4L"]
        level4A <- map["level4A"]
        level5 <- map["level5"]
        detail <- map["detail"]
    }
    
    var level0: String?
    var level1: String?
    var level2: String?
    var level3: String?
    var level4L: String?
    var level4A: String?
    var level5: String?
    var detail: String?
}

/*
 error
 */

struct AddressError: Mappable {
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        level <- map["level"]
        code <- map["code"]
        text <- map["text"]
    }
    
    var level: String?
    var code: String?
    var text: String?
}







