//
//  Strings.swift
//  DustDoIt
//
//  Created by JH on 2018. 4. 7..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation


let str_measureStation: String = "측정소"

let str_stateGood: String = "좋음"
let str_stateNormal: String = "  보통"
let str_stateBad: String = "  나쁨"
let str_stateVeryBad: String = "  매우나쁨"

let str_dustPm10: String = "미세먼지 pm10"
let str_dustPm25: String = "(초)미세먼지 pm2.5"

