//
//  Constant.swift
//  DustDoIt
//
//  Created by JH on 2018. 1. 25..
//  Copyright © 2018년 JH. All rights reserved.
//

import Foundation

public class DustNetConstant {
    
    public static let AIR_KOREA_BASE_URL: String = "http://openapi.airkorea.or.kr/openapi/services/rest/"
    
    // 미세먼지
    // 대기질 측정소 정보를 조회하기 위한 서비스로 TM 좌표기반의 가까운 측정소 및 측정소 목록과 측정소의 정보를 조회할 수 있다.
    public static let GET_NEAR_STATIONS: String = "MsrstnInfoInqireSvc/getNearbyMsrstnList"
    // 측정소명과 측정데이터 기간(일, 한달, 3개월)으로 해당 측정소의 일반항목 측정정보를 제공하는 측정소별 실시간 측정정보 조회
    public static let GET_STATION_MEASURE_INFO: String = "ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty"
    // 통보코드와 통보시간으로 예보정보와 발생 원인 정보를 조회하는 대기질(미세먼지/오존) 예보통보 조회
    public static let GET_FORECAST_INFO: String = "ArpltnInforInqireSvc/getMinuDustFrcstDspth"
    // 측정소 주소 또는 측정소 명칭으로 측정소 목록 또는 단 건의 측정소 상세 정보를 제공하는 서비스
    public static let GET_STATION_LIST_BY_NAME: String = "MsrstnInfoInqireSvc/getMsrstnList"
    
    // request parameter
    public static let PARAM_SERVICE_KEY: String = "ServiceKey"
    public static let PARAM_PAGE_NO: String = "pageNo"
    public static let PARAM_NUMOFROWS: String = "numOfRows"
    public static let PARAM_STATIONNAME: String = "stationName"
    public static let PARAM_RETURNTYPE: String = "_returnType"
    public static let PARAM_DATATERM: String = "dataTerm"
    public static let PARAM_VER: String = "ver"
    public static let PARAM_TMX: String = "tmX"
    public static let PARAM_TMY: String = "tmY"
    public static let PARAM_SEARCHDATE: String = "searchDate"
    public static let PARAM_INFORMCODE: String = "InformCode"
    
    // static request value
    public static let VALUE_DATATERM: String = "DAILY"
    public static let VALUE_JSON: String = "json"
    public static let VALUE_VER: String = "1.3"
}


/*
 weather api
 */
public class WeatherNetConstants {
    //http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastGrib?serviceKey=VUs43k8DDoTGGL9KCdi9S%2FB4ZDYZdHmD%2BQfAIxdnYBLZiLDLdj%2FAX2%2FbCwM0grhDxx3JEAH4%2BJGlcKyrifpnPQ%3D%3D&base_date=20180417&base_time=0600&nx=60&ny=127&numOfRows=10&pageSize=10&pageNo=1&startPage=1&_type=json
    
    // http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastGrib?ServiceKey=VUs43k8DDoTGGL9KCdi9S%2FB4ZDYZdHmD%2BQfAIxdnYBLZiLDLdj%2FAX2%2FbCwM0grhDxx3JEAH4%2BJGlcKyrifpnPQ%3D%3D&base_date=20180416&base_time=1500&nx=55&ny=124&_type=json
    // http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastTimeData?ServiceKey=VUs43k8DDoTGGL9KCdi9S%2FB4ZDYZdHmD%2BQfAIxdnYBLZiLDLdj%2FAX2%2FbCwM0grhDxx3JEAH4%2BJGlcKyrifpnPQ%3D%3D&base_date=20180416&base_time=0600&nx=55&ny=124&_type=json
    // http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastSpaceData?ServiceKey=VUs43k8DDoTGGL9KCdi9S%2FB4ZDYZdHmD%2BQfAIxdnYBLZiLDLdj%2FAX2%2FbCwM0grhDxx3JEAH4%2BJGlcKyrifpnPQ%3D%3D&base_date=20180416&base_time=1400&nx=55&ny=124&_type=json
    public static let WEATHER_BASE_URL: String = "http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/"
    
    // middel url
    public static let FORECAST_GRIB: String = "ForecastGrib" // 초단기 실황
    public static let FORECAST_TIMEDATA: String = "ForecastTimeData" // 초단기 예보
    public static let FORECAST_SPACEDATA: String = "ForecastSpaceData" // 동네예보 조회

    // request parameter
    public static let PARAM_SERVICE_KEY: String = "ServiceKey"
    public static let PARAM_BASE_DATE: String = "base_date" // 발표일
    public static let PARAM_BASE_TIME: String = "base_time" // 발표시각
    public static let PARAM_NX: String = "nx"
    public static let PARAM_NY: String = "ny"
    public static let PARAM_TYPE: String = "_type"
    public static let PARAM_NUMOFROWS: String = "numOfRows"
    
    // static request value
    public static let VALUE_TYPE: String = "json"
    public static let VALUE_NUMOFROWS: String = "300" // 호출때마다 갯수가 달라짐, 적절히 큰 숫자 할당
}

/*
 vworld api
 */
class VWorldNetConstant {
    public static let VWORLD_BASE_URL: String = "http://api.vworld.kr/req/"
    
    // middel url
    public static let ADRESS: String = "address"
    
    // request parameter
    public static let PARAM_SERVICE: String = "service" // 요청 서비스명
    public static let PARAM_API_KEY: String = "key" // 발급받은 api key
    public static let PARAM_REQUEST: String = "request" // 요청 서비스 오퍼레이션
    public static let PARAM_FORMAT: String = "format" // 응답결과 포맷 기본값 json
    public static let PARAM_POINT: String = "point" // 주소를 찾을 좌표 x, y
    public static let PARAM_CRS: String = "crs" // 응답결과 좌표계
    public static let PARAM_TYPE: String = "type" // 검색 주소 유형, 도로주소, 지번주소 또는 둘다 요청할 수 있습니다.
    public static let PARAM_ZIPCODE: String = "zipcode" // 우편번호 반환 여부 true(기본값), false
    public static let PARAM_SIMPLE: String = "simple" // 응답결과 간략 출력 여부 true, false(기본값)
    
    // static request value
    public static let VALUE_SERVICE: String = "address"
    public static let VALUE_REQUEST: String = "getAddress"
    public static let VALUE_FORMAT: String = "json"
    public static let VALUE_CRS: String = "EPSG:4326" // WGS84
    public static let VALUE_TYPE: String = "PARCEL"
    public static let VALUE_ZIPCODE: String = "true"
    public static let VALUE_SIMPLE: String = "false"
}

public class UseConstant {
    public static let AIR_KOREA_HOME: String = "http://www.airkorea.or.kr/index"
}









