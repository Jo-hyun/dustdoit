//
//  TodayViewController.swift
//  DustDoItTodayExtension
//
//  Created by JH on 2018. 5. 12..
//  Copyright © 2018년 JH. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreLocation

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var leftContainer: UIView!
    @IBOutlet weak var rightContainer: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    @IBOutlet weak var pm10Label: UILabel!
    @IBOutlet weak var pm25Label: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var dustIndicator: UIActivityIndicatorView!
    @IBOutlet weak var temperatureIndicator: UIActivityIndicatorView!
    
    private lazy var leftInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        let attributedText = NSMutableAttributedString(string: str_stateGood, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize, weight: .ultraLight), NSAttributedString.Key.foregroundColor: UIColor.myBlue])
        attributedText.append(NSMutableAttributedString(string: str_stateNormal, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize, weight: .ultraLight), NSAttributedString.Key.foregroundColor: UIColor.myGreen]))
        attributedText.append(NSMutableAttributedString(string: str_stateBad, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize, weight: .ultraLight), NSAttributedString.Key.foregroundColor: UIColor.myYellow]))
        attributedText.append(NSMutableAttributedString(string: str_stateVeryBad, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.bottomlabelFontSize, weight: .ultraLight), NSAttributedString.Key.foregroundColor: UIColor.red]))
        
        label.attributedText = attributedText
        label.textAlignment = .left
        
        return label
    }()
    
    private var bottomlabelFontSize: CGFloat = 10
    
    private var updateResult = NCUpdateResult.noData
    private lazy var locationMgr = CLLocationManager()
    
    private var getNearStation: GetNearStation!
    private var getMeasureInfos: GetMeasureInfos!
    private var getAddress: GetAddress!
    private var getSpaceDataForecast: GetSpaceDataForecast!
    private var getShortTermForecast: GetShortTermForecast!
    
    override func viewDidLoad() {
        logger("TodayViewController viewDidLoad")
        super.viewDidLoad()
        updateViewWith(isExistData: true)
        self.extensionContext?.widgetLargestAvailableDisplayMode = .compact
        
        self.view.addSubview(leftInfoLabel)
        leftInfoLabel.heightAnchor.constraint(equalToConstant: 12).isActive = true
        leftInfoLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 2).isActive = true
        leftInfoLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 2).isActive = true
        
        pm10Label.isHidden = true
        pm25Label.isHidden = true
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapView)))
        
        locationMgr.delegate = self
        locationMgr.desiredAccuracy = kCLLocationAccuracyBest
        locationMgr.requestLocation()
        
        dustIndicator.startAnimating()
        temperatureIndicator.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        logger("TodayViewController viewWillAppear")
        super.viewWillAppear(animated)
    }
    
    @objc func tapView() {
        if let url = URL(string: "dustdoit://") {
            self.extensionContext?.open(url, completionHandler: nil)
        }
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        logger("TodayViewController widgetPerformUpdate")
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(updateResult)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .compact {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 100)
        } else {
            self.preferredContentSize = maxSize
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateViewWith(isExistData: Bool) {
        self.noDataLabel.isHidden = isExistData
        self.leftContainer.isHidden = !isExistData
        self.rightContainer.isHidden = !isExistData
        self.leftInfoLabel.isHidden = !isExistData
    }
}



/*
 location
 */
extension TodayViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {

            let longitude = currentLocation.coordinate.longitude
            let latitude = currentLocation.coordinate.latitude

            self.getAddress(x: longitude, y: latitude)
            logger("longitude = \(longitude), latitude = \(latitude)")

            let wgs84_point = GeographicPoint(x: longitude, y: latitude)
            let converter = GeoConverter()
            let tm_point = converter.convert(sourceType: .WGS_84, destinationType: .TM, geoPoint: wgs84_point)

            if let pos = tm_point {
                self.getNearStation(tmX: pos.x, tmY: pos.y)
                logger("pos.x = \(pos.x), pos.y = \(pos.y)")
            }
        } else {
            updateViewWith(isExistData: false)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        logger("Error \(error)")
        updateViewWith(isExistData: false)
    }
}



/*
 API, callback
 */
extension TodayViewController: NetworkCallback {
    
    private func getNearStation(tmX: Double, tmY: Double) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getNearStation = GetNearStation(tmX: "\(tmX)", tmY: "\(tmY)")
            sf.getNearStation.setNetworkCallback(callback: sf, callbackName: .nearStation)
            sf.getNearStation.startRequest()
        }
    }
    
    private func getMeasureInfo(with stationName: String) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getMeasureInfos = GetMeasureInfos(stationName: stationName)
            sf.getMeasureInfos.setNetworkCallback(callback: sf, callbackName: .measureInfos)
            sf.getMeasureInfos.startRequest()
        }
    }
    
    private func getAddress(x: Double, y: Double) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getAddress = GetAddress(x: x, y: y)
            sf.getAddress.setNetworkCallback(callback: sf, callbackName: .address)
            sf.getAddress.startRequest()
        }
    }
    
    private func getSpaceDataForecast(baseDate: String, baseTime: String, x: String, y: String) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getSpaceDataForecast = GetSpaceDataForecast(baseDate: baseDate, baseTime: baseTime, x: x, y: y)
            sf.getSpaceDataForecast.setNetworkCallback(callback: sf, callbackName: .spaceDataForecast)
            sf.getSpaceDataForecast.startRequest()
        }
    }
    
    private func getShortTermForecast(baseDate: String, baseTime: String, x: String, y: String) {
        DispatchQueue.global(qos: .default).async { [weak self] in
            guard let sf = self else {return}
            sf.getShortTermForecast = GetShortTermForecast(baseDate: baseDate, baseTime: baseTime, x: x, y: y)
            sf.getShortTermForecast.setNetworkCallback(callback: sf, callbackName: .shortTermForecast)
            sf.getShortTermForecast.startRequest()
        }
    }
    
    func getResult(name: NetCallBack, result: Bool) {
        switch name {
        case .address:
            guard let data = getAddress.returnData() else {return}
            guard let result = data.response?.result else {return}
            if result.count == 0 {return}
            guard let structure = result[0].structure else {return}
            
            guard let si = structure.level1 else {return}
            guard let gu = structure.level2 else {return}
//            guard let dong = structure.level4A else {return}
//            guard let ddong = structure.level4L else {return}
//
//            self.districtName["gu"] = gu
//            self.districtName["dong"] = dong
//
//            if dong == "" {
//                self.districtName["dong"] = ddong
//            }
            
            let baseDate = Date.shortTermBaseDateWith(format: "yyyyMMdd")
            var x = "54", y = "124" // 인천 남구 격자 좌표
            if let district = DBManager.shared.getDistrictsWith(si: si, gu: gu) {
                x = district[0].x
                y = district[0].y
            }
            
            self.getShortTermForecast(baseDate: baseDate, baseTime: Date.shortTermBaseTime(), x: x, y: y)
            
        case .nearStation:
            guard let data = getNearStation.returnData() else {return}
            guard let nearStations = data.list else {return}
            if nearStations.count == 0 {return}
            
            if let stationName = nearStations[0].stationName {
                self.getMeasureInfo(with: stationName)
            } else {
                logger("no station name")
            }
            
        case .measureInfos:
            guard let data = getMeasureInfos.returnData() else {return}
            guard let measureInfos = data.list else {return}
            if measureInfos.count == 0 {return}
            
            let measureInfo = measureInfos[0]
            
            if let pm10 = measureInfo.pm10Value {
                self.pm10Label.text = pm10
                if let intPm10 = Int(pm10) {
                    self.pm10Label.textColor = UIColor.getMeasuredPm10Color(source: intPm10)
                }
                pm10Label.isHidden = false
            }
            
            if let pm10 = measureInfo.pm25Value {
                self.pm25Label.text = pm10
                if let intPm10 = Int(pm10) {
                    self.pm25Label.textColor = UIColor.getMeasuredPm25Color(source: intPm10)
                }
                pm25Label.isHidden = false
            }
            
            dustIndicator.stopAnimating()
            updateViewWith(isExistData: true)
            
            self.updateResult = .newData
            
        case .shortTermForecast:
            guard let forecast = getShortTermForecast.returnData() else {return}
            guard let response = forecast.response else {return}
            guard let body = response.body else {return}
            guard let items = body.items else {return}
            guard let item = items.item else {return}
            if item.count == 0 {return}
            
            for temp in item {
                if let category = temp.category {
                    if category == "T1H" {
                        if let t = temp.obsrValue {
                            self.temperatureLabel.text = "\(Int(t))˚"
                            
                            self.updateResult = .newData
                        }
                    }
                }
            }
            
            temperatureIndicator.stopAnimating()
            updateViewWith(isExistData: true)
            
        default:
            logger("default")
            updateViewWith(isExistData: false)
        }
    }
    
    
}










